package by.epam.myshop.dao;

import java.util.List;

import by.epam.myshop.entity.Item;
import by.epam.myshop.exception.DAOException;

public interface ItemDAO extends GenericDAO<Integer, Item> {

	public List<Item> findAll() throws DAOException;
	public Item findEntityById(Integer id) throws DAOException;
	public boolean delete(Integer id) throws DAOException;
	public boolean create(Item client) throws DAOException;
	public List<Item> findItemsByType(String type) throws DAOException;
	public boolean modifyItemInfo(Item item) throws DAOException;
	public List<Item> findItemsByOrderId(int id) throws DAOException;
}
