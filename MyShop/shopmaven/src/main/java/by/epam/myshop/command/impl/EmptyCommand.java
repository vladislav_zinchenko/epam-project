package by.epam.myshop.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.resource.ConfigurationManager;

public class EmptyCommand implements ActionCommand {
	private static final Logger log = Logger.getLogger(EmptyCommand.class);
	private static final String LOGIN_PAGE = "path.page.login";
	
	@Override
	public String execute(HttpServletRequest request) {
		log.info("EmptyCommand.execute()");
		
		String page = ConfigurationManager.getProperty(LOGIN_PAGE);
		return page;
	}
}