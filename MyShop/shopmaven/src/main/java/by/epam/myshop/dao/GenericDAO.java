package by.epam.myshop.dao;

import java.util.List;

import by.epam.myshop.entity.AbstractEntity;
import by.epam.myshop.exception.DAOException;

public interface GenericDAO<K, T extends AbstractEntity> {
	
	public List<T> findAll() throws DAOException;
	public T findEntityById(K id) throws DAOException;
	public boolean delete(K id) throws DAOException;
	public boolean create(T entity) throws DAOException;
	
}
