package by.epam.myshop.dao;

import java.util.List;

import by.epam.myshop.entity.Client;
import by.epam.myshop.exception.DAOException;

public interface ClientDAO extends GenericDAO<String, Client> {
	
	public List<Client> findAll() throws DAOException;
	public Client findEntityById(String login) throws DAOException;
	public boolean delete(String login) throws DAOException;
	public boolean create(Client client) throws DAOException;
	public boolean signUp(Client client) throws DAOException;
	public boolean isExist(String login) throws DAOException;
	public Client findClientByLoginAndPassword(String login, String password) throws DAOException;
	public boolean changeBlackListStatus(String login, int status) throws DAOException;
	

}
