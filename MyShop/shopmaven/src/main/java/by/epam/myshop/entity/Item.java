package by.epam.myshop.entity;

@SuppressWarnings("serial")
public class Item extends AbstractEntity {
	
	private int itemId;
	private String itemName;
	private int itemsAmount;
	private String manufacterName;
	private String itemDescription;
	private String itemPicPath;
	private String itemType;
	
	public Item() { }

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemsAmount() {
		return itemsAmount;
	}

	public void setItemsAmount(int itemsAmount) {
		this.itemsAmount = itemsAmount;
	}

	public String getManufacterName() {
		return manufacterName;
	}

	public void setManufacterName(String manufacterName) {
		this.manufacterName = manufacterName;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemPicPath() {
		return itemPicPath;
	}

	public void setItemPicPath(String itemPicPath) {
		this.itemPicPath = itemPicPath;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemDescription == null) ? 0 : itemDescription.hashCode());
		result = prime * result + itemId;
		result = prime * result + ((itemName == null) ? 0 : itemName.hashCode());
		result = prime * result + ((itemPicPath == null) ? 0 : itemPicPath.hashCode());
		result = prime * result + ((itemType == null) ? 0 : itemType.hashCode());
		result = prime * result + itemsAmount;
		result = prime * result + ((manufacterName == null) ? 0 : manufacterName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (itemDescription == null) {
			if (other.itemDescription != null)
				return false;
		} else if (!itemDescription.equals(other.itemDescription))
			return false;
		if (itemId != other.itemId)
			return false;
		if (itemName == null) {
			if (other.itemName != null)
				return false;
		} else if (!itemName.equals(other.itemName))
			return false;
		if (itemPicPath == null) {
			if (other.itemPicPath != null)
				return false;
		} else if (!itemPicPath.equals(other.itemPicPath))
			return false;
		if (itemType == null) {
			if (other.itemType != null)
				return false;
		} else if (!itemType.equals(other.itemType))
			return false;
		if (itemsAmount != other.itemsAmount)
			return false;
		if (manufacterName == null) {
			if (other.manufacterName != null)
				return false;
		} else if (!manufacterName.equals(other.manufacterName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Item [itemId=" + itemId + ", itemName=" + itemName + ", itemsAmount=" + itemsAmount
				+ ", manufacterName=" + manufacterName + ", itemDescription=" + itemDescription + ", itemPicPath="
				+ itemPicPath + ", itemType=" + itemType + "]";
	}
}
