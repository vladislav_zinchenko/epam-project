package by.epam.myshop.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ItemDAO;
import by.epam.myshop.dao.impl.ItemDAOImpl;
import by.epam.myshop.entity.Item;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class TakeItemsByTypeCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(TakeItemsByTypeCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String ITEMS = "items";
	private final String ITEM_TYPE = "item_type";
	private final String ERROR_PAGE = "path.page.error";
	private final String ITEMS_PAGE = "path.page.items";
	
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("TakeItemsByTypeCommand.execute()");
		
		String page = null;
		String type = request.getParameter(ITEM_TYPE);
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		try {
			List<Item> items = itemDAO.findItemsByType(type);
			if (items.isEmpty()) {
				request.setAttribute(MESSAGE, MessageManager.SEARCHING_ITEM_NOT_FOUND);
				request.getSession().setAttribute(ITEMS, null);
				page = ConfigurationManager.getProperty(ITEMS_PAGE);
			} else {
				request.getSession().setAttribute(ITEMS, items);
				page = ConfigurationManager.getProperty(ITEMS_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		
		request.getSession().setAttribute(URL, page);
		return page;
	}

}
