package by.epam.myshop.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.myshop.dao.ClientDAO;
import by.epam.myshop.dao.pool.DBConnectionPool;
import by.epam.myshop.entity.Client;
import by.epam.myshop.exception.ConnectionPoolException;
import by.epam.myshop.exception.DAOException;

public class ClientDAOImpl implements ClientDAO {
	private static ClientDAOImpl instance = new ClientDAOImpl();  
	
	private static final String CLIENT_LOGIN = "client.login";
	private static final String CLIENT_PASSWORD = "client.password";
	private static final String CLIENT_ID = "client.client_id";
	private static final String CLIENT_NAME = "client.name";
	private static final String CLIENT_SURNAME = "client.surname";
	private static final String CLIENT_STATUS = "client.is_admin";
	private static final String CLIENT_STATUS_BL = "client.is_black";
	private final static String SQL_SIGNUP_CLIENT = 
			"INSERT INTO shop.client(client_id, login, password, name, surname, is_admin, is_black) VALUES(?, ?, ?, ?, ?, ?, ?)";
	private final static String SQL_IS_CLIENT_EXISTS = 
			"SELECT * FROM shop.client WHERE login = ?";
	private final static String SQL_FIND_CLIENT_BY_LOGIN_AND_PASSWORD = 
			"SELECT * FROM shop.client WHERE login = ? AND password = ?";
	private final static String SQL_FIND_ALL_CLIENTS = 
			"SELECT * FROM shop.client";
	private final static String SQL_DELETE_CLIENT = 
			"DELETE FROM shop.client WHERE client.login = ?";
	private final static String SQL_UPDATE_BLACKLIST_STATUS = 
			"UPDATE shop.client SET is_black = ? WHERE client.login = ?";
	
	private static final Logger LOG = Logger.getLogger(ClientDAOImpl.class);
	
	private ClientDAOImpl() {
		super();
	}
	
	public static ClientDAO getInstance() {
		LOG.info("ClientDAOImpl.getInstance");
		return instance;
	}

	@Override
	public List<Client> findAll() throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance();  
		
		ArrayList<Client> clients = new ArrayList<Client>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_FIND_ALL_CLIENTS);			
			ResultSet resultSet = prStatement.executeQuery();
			while(resultSet.next()) {
				Client client = new Client();
				client.setLogin(resultSet.getString(CLIENT_LOGIN));
				client.setPassword(resultSet.getString(CLIENT_PASSWORD));
				client.setId(resultSet.getInt(CLIENT_ID));
				client.setName(resultSet.getString(CLIENT_NAME));
				client.setSurname(resultSet.getString(CLIENT_SURNAME));
				client.setStatus(resultSet.getInt(CLIENT_STATUS));
				client.setStatusBL(resultSet.getInt(CLIENT_STATUS_BL));
				
				clients.add(client);
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException();
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                } 
				pool.closeConnection(connection);
				connection.close();
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		}
		return clients;
	}
	
	public boolean signUp(Client client) throws DAOException {
		LOG.info("ClientDAO.signUp()");
		
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean flag = false;
		if (isExist(client.getLogin()) || client == null) {
			LOG.info(2);
			return flag;
		}
		LOG.info(3);
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			LOG.info(4);
			prStatement = connection.prepareStatement(SQL_SIGNUP_CLIENT);
			LOG.info(5);
			prStatement.setInt(1, client.getId()); ////!!!!!
			prStatement.setString(2, client.getLogin());
			prStatement.setString(3, client.getPassword());
			prStatement.setString(4, client.getName());
			prStatement.setString(5, client.getSurname());
			prStatement.setInt(6, client.getStatus());
			prStatement.setInt(7, client.getStatusBL());
			if(prStatement.executeUpdate() > 0) {
				flag = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return flag;
	}
	
	public boolean isExist(String login) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_IS_CLIENT_EXISTS);
			prStatement.setString(1, login);
			ResultSet resultSet;
			resultSet = prStatement.executeQuery();
			result = resultSet.first();
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException();
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}
	
	public Client findClientByLoginAndPassword(String login, String password) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		Client client = null;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_FIND_CLIENT_BY_LOGIN_AND_PASSWORD);
			prStatement.setString(1, login);
			prStatement.setString(2, password);
			ResultSet resultSet = prStatement.executeQuery();
			if (resultSet.next()) {
				client = new Client();
				client.setLogin(resultSet.getString(CLIENT_LOGIN));
				client.setPassword(resultSet.getString(CLIENT_PASSWORD));
				client.setName(resultSet.getString(CLIENT_NAME));
				client.setSurname(resultSet.getString(CLIENT_SURNAME));
				client.setStatus(resultSet.getInt(CLIENT_STATUS));
				client.setId(resultSet.getInt(CLIENT_ID));
				client.setStatusBL(resultSet.getInt(CLIENT_STATUS_BL));
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return client;
	}

	@Override
	public boolean delete(String login) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_DELETE_CLIENT);
			prStatement.setString(1, login);
			if (prStatement.executeUpdate() > 0) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}
	
	public boolean changeBlackListStatus(String login, int status) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_UPDATE_BLACKLIST_STATUS);
			prStatement.setInt(1, status);
			prStatement.setString(2, login);
			if (prStatement.executeUpdate() == 1) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}

	@Override
	public Client findEntityById(String login) throws DAOException {
		try {
	        throw new UnsupportedOperationException();
	    } catch (UnsupportedOperationException e) {
	    	LOG.error("UnsupportedOperationException", e);
	    }
		return null;
	}

	@Override
	public boolean create(Client entity) throws DAOException {
		try {
	        throw new UnsupportedOperationException();
	    } catch (UnsupportedOperationException e) {
	    	LOG.error("UnsupportedOperationException", e);
	    }
		return false;
	}

}
