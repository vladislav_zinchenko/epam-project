package by.epam.myshop.filter;


import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;


public class EncodingFilter implements Filter {
	private static final Logger LOG = Logger.getLogger(EncodingFilter.class);
	
	private String encoding;
	
	public EncodingFilter() {}
	
	@Override
	public void init(FilterConfig config) {
		LOG.info("EncodingFilter.init()");
		encoding = config.getInitParameter("encoding");
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		LOG.info("EncodingFilter.doFilter()");
		String requestEncoding = request.getCharacterEncoding();
		if (encoding != null && !encoding.equalsIgnoreCase(requestEncoding)) {
			request.setCharacterEncoding(encoding);
			response.setCharacterEncoding(encoding);
		}
		chain.doFilter(request, response);
	}
	
	@Override
	public void destroy() { 
		LOG.info("EncodingFilter.destroy()");
	}
	


}
