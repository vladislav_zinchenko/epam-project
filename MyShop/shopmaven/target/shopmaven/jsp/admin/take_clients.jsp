<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.admin.take_clients" /></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" >
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/admin.css" rel="stylesheet" type="text/css">
	
  </head>

  <body>
  
    <!-- Navigation -->
	<%@include file="/jsp/jspf/ind_nav.jspf"%>
	<!---->
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->
	
	<div class="container">             
            <div class="row">
				<c:forEach var="client" items="${clients}">
                    <div class="col-xs-12 col-sm-4" >
                        <div class="thumbnail" id="border">
                            <div class="caption">
									<h5>
										<fmt:message key="info.client.id"/>
										${client.id}
									</h5>
					
									<h5>
										<fmt:message key="info.client.login"/>
										${client.login}
									</h5>
									<h5>
										<fmt:message key="info.client.name"/>
										${client.name}
									</h5>
									<h5>
										<fmt:message key="info.client.surname"/>
										${client.surname}
									</h5>
									<h5>
										<fmt:message key="info.client.status"/>
										${client.status}
									</h5>
									<h5>
										<fmt:message key="info.client.statusBL"/>
										${client.statusBL}
									</h5>
								<c:choose>
									<c:when test="${client.status == 1}">
										<h5><fmt:message key="static.status.admin"/></h5>
									</c:when>
									<c:otherwise>
										<h5><fmt:message key="static.status.notAdmin"/></h5>
									</c:otherwise>
								</c:choose>
								
								<form class="form" name="DeleteClientForm" method="POST" action="Controller">
									<input type="hidden" name="command" value="delete_client"/>
									<input type="hidden" name="login" value="${client.login}"/>
									<input type="hidden" name="status" value="${client.status}"/>
									<input class="btn btn-default" type="submit" value="<fmt:message key="button.delete"/> "/>
								</form>

								<form class="form" name="AddToBlackListForm" method="POST" action="Controller">
									<input type="hidden" name="command" value="blacklist_client"/>
									<input type="hidden" name="status_BL" value="${client.statusBL}"/>
									<input type="hidden" name="status" value="${client.status}"/>
									<input type="hidden" name="login" value="${client.login}"/>
									<input class="btn btn-default" type="submit" value="<fmt:message key="button.blacklist"/> "/>
								</form>
								
                            </div>
                        </div>    
                    </div> 
				</c:forEach>					
                    
            </div>   
    </div>

	
	 <%@include file="/jsp/jspf/footer.jspf"%>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
	
	</body>
</html>