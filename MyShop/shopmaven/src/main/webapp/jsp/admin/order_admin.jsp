<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>

<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.items"/></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
    <script src="js/ie-emulation-modes-warning.js"></script>
	
  </head>

  <body>
	<!-- Navigation for user-->
	<%@include file="/jsp/jspf/ind_nav.jspf"%>
	<!---->
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->


	<div class="container">
	<!--<c:choose>
		<c:when test="${not empty sessionScope.orders}">-->



		<!--<c:out value="${amountMap['key']}"/>-->


	    <div class="row">
			<c:forEach var="order" items="${sessionScope.orders}">
							<div class="col-12 ">
								<div class="thumbnail" id="border">

				                            <div class="caption">
				                            <fmt:message key="info.order"/>
				                            <hr>
				                                <h3 id="font"><fmt:message key="info.order.id"/> ${order.orderId}</h3>
												<h3 id="font"><fmt:message key="info.order.date"/> ${order.date}</h3>
												<h3 id="font"><fmt:message key="info.order.client"/> ${order.client.login}</h3>
												<h3 id="font"><fmt:message key="info.order.client.status"/> ${order.status}</h3>

											</div>
									<hr>
									<fmt:message key="info.items"/>

									<hr>
									<div class="container-fluid" >		
										<c:forEach var="item" items="${order.items}">	
											<div class="col-md-3">
						                        <div class="thumbnail" >
						                            <div class="caption">														
															<h5><fmt:message key="info.item.id"/> ${item.itemId}</h5>
															<h5><fmt:message key="info.item.name"/> ${item.itemName}</h5>
															<h5><fmt:message key="info.item.amount_shop"/> ${item.itemsAmount}</h5>
															<h5><fmt:message key="info.item.amount_order"/> ${order.amountMap[item.itemId]}</h5>													
													</div>
												</div>
											</div>
										</c:forEach>
									</div>

													
													<form action="Controller" method="post" align="center">
														<input name="command" type="hidden" value="finish_order" /> 
														<input name="order_id" type="hidden" value="${order.orderId}" />
														<input class="btn btn-default" type="submit" name="finish_order" value="<fmt:message key="button.finish_order" />" />								
													</form>
													<form action="Controller" method="post" align="center">
														<input name="command" type="hidden" value="delete_order" /> 
														<input name="order_id" type="hidden" value="${order.orderId}" />
														<input class="btn btn-default" type="submit" value="<fmt:message key="button.cancel_order" />" />
													</form>
											
			                    </div> 
			                </div>
			</c:forEach>					        
        </div>
        <!--</c:when>
											<c:otherwise>
												<h5 align="center" id="message">
													<fmt:message key="message.basket.no_orders" />
												</h5>
											</c:otherwise>   
        </c:choose>-->
    </div>

	
	 <%@include file="/jsp/jspf/footer.jspf"%>
  </body>
</html>