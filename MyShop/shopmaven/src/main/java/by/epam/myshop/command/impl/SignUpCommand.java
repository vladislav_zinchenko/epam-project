package by.epam.myshop.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.exception.DAOException;
import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ClientDAO;
import by.epam.myshop.dao.impl.ClientDAOImpl;
import by.epam.myshop.entity.Client;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;
import by.epam.myshop.util.Coder;
import by.epam.myshop.util.Validator;

public class SignUpCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(SignUpCommand.class);
	private final String LOGIN = "login";
	private final String PASSWORD = "password";
	private final String NAME = "name";
	private final String SURNAME = "surname";
	private final String CLIENT = "client";
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String SIGNUP_PAGE = "path.page.signup";
	private final String MAIN_PAGE = "path.page.main";
	private final String ERROR_PAGE = "path.page.error";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("SignUpCommand.execute()");
		
		String page = null;
		try {
			ClientDAO clientDAO = ClientDAOImpl.getInstance();
			String login;
			String password;
			String name;
			String surname;
			
			login = request.getParameter(LOGIN).trim();
			password = request.getParameter(PASSWORD).trim();
			name = request.getParameter(NAME).trim();
			surname = request.getParameter(SURNAME).trim();

			if (!Validator.checkEmptyField(login, password, name, surname)) {
				request.setAttribute(MESSAGE, MessageManager.SIGNUP_ERROR);
				page = ConfigurationManager.getProperty(SIGNUP_PAGE);
				return page;
			}

			Client client = new Client(login, password, name, surname, 0, 0); 
			client.setId(Math.abs(client.hashCode()));
			
			String check = Validator.checkClientInfo(client);
			LOG.info(check);
			
			if (check == null) {
				client.setPassword(Coder.hashMD5(password));	
				boolean result = clientDAO.signUp(client);	
				
				if (!result) {
					request.setAttribute(MESSAGE, MessageManager.SIGNUP_EXIST_ERROR);
					page = ConfigurationManager.getProperty(SIGNUP_PAGE);
				} else {
					request.getSession().setAttribute(CLIENT, client);
					request.setAttribute(MESSAGE, MessageManager.SIGNUP_SUCCESS);
					page = ConfigurationManager.getProperty(MAIN_PAGE);
				}		
			} else {
				request.setAttribute(MESSAGE, check);
				page = ConfigurationManager.getProperty(SIGNUP_PAGE);
			}
		} catch (DAOException e) { 
			LOG.info("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
			return page;
		}	
		request.getSession().setAttribute(URL, page);
		return page;
	}
}