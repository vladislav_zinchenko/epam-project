<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>

<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.items"/></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
    <script src="js/ie-emulation-modes-warning.js"></script>
	
  </head>

  <body>
	<!-- Navigation for user-->
	<%@include file="/jsp/jspf/ind_nav.jspf"%>
	<!---->
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->
<div class="container">
	<c:forEach var="order" items="${sessionScope.orders}">
		<c:choose>
			<c:when test="${not empty sessionScope.orders}">
                    <div class="col-xs-12 col-sm-12">
                        <div class="thumbnail" id="border">
                            <div class="caption">
                            	<fmt:message key="info.order"/>
				                            <hr>
				                                <h3><fmt:message key="info.order.id"/> ${order.orderId}</h3>
												<h3><fmt:message key="info.order.date"/> ${order.date}</h3>
												<h3><fmt:message key="info.order.client"/> ${order.client.login}</h3>
												<h3><fmt:message key="info.order.client.status"/> ${order.status}</h3>

							</div>
									<hr>
								<!-- Show items in order-->
								
								<fmt:message key="info.items"/>
								
								<hr>
								<div class="container-fluid" >
									
									<c:forEach var="item" items="${order.items}">	
										<div class="col-md-3">
					                        <div class="thumbnail" >
					                            <div class="caption">
													<h5><fmt:message key="info.item.id"/> ${item.itemId}</h5>
													<h5><fmt:message key="info.item.name"/> ${item.itemName}</h5>
													<h5><fmt:message key="info.item.amount_shop"/> ${item.itemsAmount}</h5>
													<h5><fmt:message key="info.item.amount_order"/> ${order.amountMap[item.itemId]}</h5>	
												</div>
											</div>
										</div>
									</c:forEach>
								</div>
									<c:choose>
										<c:when test="${order.status == 1}">
											<fmt:message key="order-completed" />
										</c:when>
										<c:otherwise>
											<form action="Controller" method="post" align="center">
												<input class="btn btn-default" type="submit" value="<fmt:message key="cancel-order" />" />
												<input type="hidden" name="command"  value="delete_order" />
												<input type="hidden" name="order_id" value="${order.orderId}" /> 
										</c:otherwise>
									</c:choose>	
		
										<!--jspf order info-->
							</div>
						</div>
					</div>
			</c:when>
			<c:otherwise>
				
			</c:otherwise>		
		</c:choose>					
	</c:forEach>	
</div>

<!--
Vertical list of orders
<div class="container" >
    <ul class="list-group">
    <li>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="panel-info">
                    <p><strong>Tel: 011-345-898</strong></p>
                    <p>9182 Lorem Ipsum<br />
                        consectetur adipiscing elit.<br />
                       Nullam vel lacus felis.</p>
                </div>
                <div class="panel-rating">
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <p>Rate Yourself!</p>
                </div>
                <div class="panel-more1">
                    <img src="http://lorempixel.com/100/100" />
                    <br /><span>Caption</span>
                </div>
                <div class="panel-more1">
                    <img src="http://lorempixel.com/100/100" />
                    <br /><span>Caption</span>
                </div>
                <div class="panel-more1">
                    <img src="http://lorempixel.com/100/100" />
                    <br /><span>Caption</span>
                </div>
            </div>
        </div>
    </li>
</ul>
</div>	
-->







	 <%@include file="/jsp/jspf/footer.jspf"%>
  </body>
</html>