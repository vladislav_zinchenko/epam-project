<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>

<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.items"/></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
    <script src="js/ie-emulation-modes-warning.js"></script>
	
  </head>

  <body>
	<!-- Navigation for user-->
	<%@include file="/jsp/jspf/ind_nav.jspf"%>
	<!---->
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->

<div class="container">
            <div class="row">
				<c:forEach var="item" items="${sessionScope.basket}">
                    <div class="col-xs-6 col-sm-4">
                        <div class="thumbnail">
                            <img src="${item.itemPicPath}" alt="" width="280" height="280"/>
	                            <div class="caption" id="margin">
	                                <h3 id="font"><fmt:message key="info.item.name"/> ${item.itemName}</h3>
									<h3 id="font"><fmt:message key="info.item.id"/> ${item.itemId}</h3>
									<h3 id="font"><fmt:message key="info.item.amount_shop"/> ${item.itemsAmount}</h3>
									<h3 id="font"><fmt:message key="info.item.amount_order"/> ${sessionScope.amount_map[item.itemId]}</h3>
									<h3 id="font"><fmt:message key="info.item.type"/> ${item.itemType}</h3>
									<h3 id="font"><fmt:message key="info.item.manufacter"/> ${item.manufacterName}</h3>
	                                <h3 id="font"><fmt:message key="info.item.desc"/> ${item.itemDescription}</p>
								</div> 
				
								<form action="Controller" method="post" align="center">
									<input class="btn btn-default" type="submit" value="<fmt:message key="button.item.delete" />" /> 
									<input type="hidden" name="item_id" value="${item.itemId}" />
									<input type="hidden" name="basket" value="${sessionScope.basket}" /> 
									<input type="hidden" name="basket" value="${sessionScope.amount_map}" /> 
									<input type="hidden" name="amount" value="${item.itemsAmount}" />
									<input type="hidden" name="command" value="delete_item_from_basket" />
								</form>
                        </div> 
                    </div> 
				</c:forEach>					        
            </div>   
     </div>


     <div class="container">
				<c:choose>
						<c:when test="${not empty sessionScope.basket}">
								<form>
									<input class="btn btn-default" type="submit" name="order_items" value="<fmt:message key="button.order_items" />" /> 
									<input type="hidden" name="basket" value="${sessionScope.basket}" /> 
									<input type="hidden" name="client" value="${sessionScope.client}" />
									<input type="hidden" name="command" value="order_items" />
								</form>
						</c:when>
				<c:otherwise>
				
			<h5 align="center" id="message">
				<fmt:message key="message.basket.no_orders" />
			</h5>

		</c:otherwise>
	</c:choose>
	<div>



	 <%@include file="/jsp/jspf/footer.jspf"%>
  </body>
</html>