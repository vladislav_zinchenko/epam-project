package by.epam.myshop.command.admin;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ClientDAO;
import by.epam.myshop.dao.OrderDAO;
import by.epam.myshop.dao.impl.ClientDAOImpl;
import by.epam.myshop.dao.impl.OrderDAOImpl;
import by.epam.myshop.entity.Client;
import by.epam.myshop.entity.Order;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class DeleteClientCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(DeleteClientCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String LOGIN = "login";
	private final String STATUS = "status";
	private final String ADMIN_TAKE_CLIENTS_PAGE = "path.page.admin.take_clients";
	private final String MAIN_PAGE = "path.page.main";
	private final String ERROR_PAGE = "path.page.error";
	
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("DeleteClientCommand.execute()");
		
		String page = null;
		String login = request.getParameter(LOGIN);
		int status =  Integer.parseInt(request.getParameter(STATUS));
		Client client = null;
		
		ClientDAO clientDAO = ClientDAOImpl.getInstance();
		OrderDAO orderDAO = OrderDAOImpl.getInstance();
		try {
			ArrayList<Order> orders = orderDAO.takeOrderByLogin(login, client);
			
			if (!clientDAO.isExist(login)) {
				LOG.info("not exist");
				request.setAttribute(MESSAGE, MessageManager.DELETE_DELETED_CLIENT_ERROR);
				page = ConfigurationManager.getProperty(MAIN_PAGE);
			} else {
				LOG.info("exist");
				if(status == 1){
					request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_ADMIN);
					page = ConfigurationManager.getProperty(ADMIN_TAKE_CLIENTS_PAGE);
				}
				else if(!orders.isEmpty()){
					request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_CLIENT_HAVE_ORDER);
					page = ConfigurationManager.getProperty(ADMIN_TAKE_CLIENTS_PAGE);
				}
				else if (clientDAO.delete(login)) {
					request.setAttribute(MESSAGE, MessageManager.DELETE_CLIENT_SUCCESS);
					page = ConfigurationManager.getProperty(MAIN_PAGE);
				} else {
					request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_CLIENT);
					page = ConfigurationManager.getProperty(ADMIN_TAKE_CLIENTS_PAGE);
				}
			}
		} catch (DAOException e) {
			LOG.error(e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		
		request.getSession().setAttribute(URL, page);
		return page;
	}

}
