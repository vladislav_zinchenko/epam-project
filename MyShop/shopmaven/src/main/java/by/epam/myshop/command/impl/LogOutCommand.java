package by.epam.myshop.command.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ItemDAO;
import by.epam.myshop.dao.impl.ItemDAOImpl;
import by.epam.myshop.entity.Item;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class LogOutCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(LogOutCommand.class);
	private final String MESSAGE = "message";
	private final String INDEX_PAGE = "path.page.index";
	private final String ERROR_PAGE = "path.page.error";
	private final String BASKET = "basket";
	private final String AMOUNT_MAP = "amount_map";
	
	
	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("LogOutCommand.execute()");
		
		String page = ConfigurationManager.getProperty(INDEX_PAGE);
		List<Item> basket = (List<Item>) request.getSession().getAttribute(BASKET);
		Map<Integer, Integer> amountMap = (HashMap<Integer, Integer>) request.getSession().getAttribute(AMOUNT_MAP);
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		try {
			if (basket != null) {
				for (Item item : basket) {
					item.setItemsAmount(item.getItemsAmount() + amountMap.get(item.getItemId()));
					itemDAO.modifyItemInfo(item);
				}
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		basket = null;
		
		request.getSession().invalidate();
		return page;
	}
}
