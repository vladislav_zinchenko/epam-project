package by.epam.myshop.command.factory;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.command.enumeration.CommandEnum;
import by.epam.myshop.command.impl.EmptyCommand;

public class ActionFactory {
	
	private static final Logger LOG = Logger.getLogger(ActionFactory.class);
	private final String COMMAND = "command";
	
	public ActionCommand defineCommand(HttpServletRequest request) throws IllegalArgumentException {
		LOG.info("ActionFactory.defineCommand()");
		
	    ActionCommand current = null;
	    String action = request.getParameter(COMMAND);
	    if (action == null || action.isEmpty()) {
	      return new EmptyCommand();
	    }
	    CommandEnum currentEnum = CommandEnum.valueOf(action.toUpperCase());
	    current = currentEnum.getCurrentCommand();
	    return current;
	  }
}
