package by.epam.myshop.entity;

import java.util.HashMap;
import java.util.List;

@SuppressWarnings("serial")
public class Order extends AbstractEntity {
	
	private int orderId;
	private String date;
	private int status;
	private Client client;
	List<Item> items;
	/////////////////////
	private HashMap<Integer, Integer> amountMap;
	
	public Order() { }

	public Order(int orderId, String date, int status, Client client, List<Item> items,
			HashMap<Integer, Integer> amountMap) {
		super();
		this.orderId = orderId;
		this.date = date;
		this.status = status;
		this.client = client;
		this.items = items;
		this.amountMap = amountMap;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

	public HashMap<Integer, Integer> getAmountMap() {
		return amountMap;
	}

	public void setAmountMap(HashMap<Integer, Integer> amountMap) {
		this.amountMap = amountMap;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amountMap == null) ? 0 : amountMap.hashCode());
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + orderId;
		result = prime * result + status;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (amountMap == null) {
			if (other.amountMap != null)
				return false;
		} else if (!amountMap.equals(other.amountMap))
			return false;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (orderId != other.orderId)
			return false;
		if (status != other.status)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Order [orderId=" + orderId + ", date=" + date + ", status=" + status + ", client=" + client + ", items="
				+ items + ", amountMap=" + amountMap + "]";
	}

	

}
