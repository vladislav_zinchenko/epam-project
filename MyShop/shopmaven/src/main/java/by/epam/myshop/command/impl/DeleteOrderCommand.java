package by.epam.myshop.command.impl;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ItemDAO;
import by.epam.myshop.dao.OrderDAO;
import by.epam.myshop.dao.impl.ItemDAOImpl;
import by.epam.myshop.dao.impl.OrderDAOImpl;
import by.epam.myshop.entity.Item;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class DeleteOrderCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(DeleteOrderCommand.class);

	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String ORDER_ID = "order_id";
	private final String AMOUNT_MAP = "amount_map";
	private final String MAIN_PAGE = "path.page.main";
	private final String ERROR_PAGE = "path.page.error";	

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("DeleteOrderCommand.execute()");
		
		String page = null;
		int orderId = Integer.parseInt(request.getParameter(ORDER_ID));
		HashMap<Integer, Integer> amountMap = new HashMap<>();
		
		OrderDAO orderDAO = OrderDAOImpl.getInstance();
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		
		try {
			if (orderDAO.getIOAmountAndItemId(orderId, amountMap)) {
				ArrayList<Item> items = (ArrayList<Item>) itemDAO.findItemsByOrderId(orderId);
				for (Item item : items) {
					item.setItemsAmount(item.getItemsAmount() + amountMap.get(item.getItemId()));
					itemDAO.modifyItemInfo(item);
					amountMap.remove(item.getItemId());
				}
				request.getSession().setAttribute(AMOUNT_MAP, amountMap);
			}
			
			if (orderDAO.delete(orderId)) {
				request.setAttribute(MESSAGE, MessageManager.DELETE_ORDER_SUCCESS);
				page = ConfigurationManager.getProperty(MAIN_PAGE);
			} else {
				request.setAttribute(MESSAGE, MessageManager.ERROR_DELETE_ORDER);
				page = ConfigurationManager.getProperty(MAIN_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}

		request.getSession().setAttribute(URL, page);
		return page;
	}
}
