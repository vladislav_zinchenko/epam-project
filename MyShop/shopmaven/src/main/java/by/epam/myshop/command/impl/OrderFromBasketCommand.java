package by.epam.myshop.command.impl;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.OrderDAO;
import by.epam.myshop.dao.impl.OrderDAOImpl;
import by.epam.myshop.entity.Client;
import by.epam.myshop.entity.Item;
import by.epam.myshop.entity.Order;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class OrderFromBasketCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(OrderFromBasketCommand.class);
	private final String CLIENT = "client";
	private final String URL = "url";
	private final String MESSAGE = "message";
	private final String AMOUNT = "amount";
	private final String BASKET = "basket";
	private final String MAIN_PAGE = "path.page.main";
	private final String ERROR_PAGE = "path.page.error";
	private final String AMOUNT_MAP = "amount_map";

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = null;
		List<Item> items = (List<Item>) request.getSession().getAttribute(BASKET);
		Client client = (Client) request.getSession().getAttribute(CLIENT);
		HashMap<Integer, Integer> amountMap = (HashMap<Integer, Integer>) request.getSession().getAttribute(AMOUNT_MAP);
		
		OrderDAO orderDAO = OrderDAOImpl.getInstance();
		Order order = new Order();
		order.setItems(items);
		order.setClient(client);
		order.setStatus(0);
		order.setAmountMap(amountMap);
		order.setOrderId(Math.abs(order.hashCode()));
		try {
			if (!orderDAO.isExist(order.getOrderId())) {
				if (orderDAO.create(order)) {
					request.getSession().setAttribute(BASKET, null);
					request.getSession().setAttribute(AMOUNT_MAP, null);
					request.setAttribute(MESSAGE, MessageManager.MAKE_ORDER_SUCCESS);
					page = ConfigurationManager.getProperty(MAIN_PAGE);
					request.getSession().setAttribute(AMOUNT, 0);
				} else {
					request.setAttribute(MESSAGE, MessageManager.MAKE_ORDER_ERROR);
					page = ConfigurationManager.getProperty(MAIN_PAGE);
				}
			} else {
				request.setAttribute(MESSAGE, MessageManager.CREATE_ORDER_EXISTS_ERROR);
				page = ConfigurationManager.getProperty(MAIN_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}

}
