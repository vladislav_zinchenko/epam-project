package by.epam.myshop.command.admin;


import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ItemDAO;
import by.epam.myshop.dao.impl.ItemDAOImpl;
import by.epam.myshop.entity.Item;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;
import by.epam.myshop.util.Validator;

public class AddItemCommand implements ActionCommand {
	private static final Logger log = Logger.getLogger(AddItemCommand.class);
	
	private final String URL = "url";
	private final String NAME = "name";
	private final String TYPE = "type";
	private final String AMOUNT = "amount";
	private final String DESCRIPTION = "description";
	private final String PATH = "path";
	private final String MANUFACTER = "manufacter";
	private final String MESSAGE = "message";
	private final String ITEM = "item";
	private final String ITEM_ADD_ADMIN_PAGE = "path.page.admin.add_item";
	private final String MAIN_PAGE = "path.page.main";
	
	@Override 
	public String execute(HttpServletRequest request) {
		log.info("AddItemCommand.execute()");
		
		String page = null;
		String itemName = request.getParameter(NAME).trim();
		String type = request.getParameter(TYPE).trim();
		int amount = Integer.parseInt(request.getParameter(AMOUNT).trim());
		String description = request.getParameter(DESCRIPTION).trim();
		String path = request.getParameter(PATH).trim();
		String manufacter = request.getParameter(MANUFACTER).trim();
		
		if (!Validator.checkEmptyField(itemName, manufacter, description, path, itemName)) {
			request.setAttribute(MESSAGE, MessageManager.ADD_ITEM_ERROR);
			page = ConfigurationManager.getProperty(ITEM_ADD_ADMIN_PAGE);
			request.getSession().setAttribute(URL, page);
			return page;
		}
		
		Item item = new Item();
		item.setItemName(itemName);
		item.setItemsAmount(amount);
		item.setManufacterName(manufacter);
		item.setItemPicPath(path);
		item.setItemDescription(description);
		item.setItemType(type);
		item.setItemId(Math.abs(item.hashCode()));
		
		String check = Validator.checkItemInfo(item);
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		try {
			if (check == null) {
				if (itemDAO.findEntityById(item.getItemId()) == null) {
					if (itemDAO.create(item)) {
						request.setAttribute(ITEM, item);
						page = ConfigurationManager.getProperty(MAIN_PAGE);
						request.setAttribute(MESSAGE, MessageManager.SUCCESFUL_ITEM_ADD_BY_ADMIN);
					} else {
						request.setAttribute(MESSAGE, MessageManager.ADD_ITEM_BY_ADMIN_ERROR);
						page = ConfigurationManager.getProperty(MAIN_PAGE);
					}
				} else {
					request.setAttribute(MESSAGE, MessageManager.ADDING_ITEM_EXISTS_ERROR);
					page = ConfigurationManager.getProperty(MAIN_PAGE);
				}
			} else {
				request.setAttribute(MESSAGE, MessageManager.ILLEGAL_ITEM_INPUT_DATA);
				page = ConfigurationManager.getProperty(MAIN_PAGE);
			}
		} catch (DAOException e) {
			log.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(MAIN_PAGE);
		}
		
		request.getSession().setAttribute(URL, page);
		return page;
	}
}
