package by.epam.myshop.command.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.OrderDAO;
import by.epam.myshop.dao.impl.OrderDAOImpl;
import by.epam.myshop.entity.Client;
import by.epam.myshop.entity.Order;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class TakeOrdersCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(TakeOrdersCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String STATUS = "status";
	private final String ORDERS = "orders";
	private final String CLIENT = "client";
	private final String ORDER_CLIENT_PAGE = "path.page.order.client";
	private final String ORDER_ADMIN_PAGE = "path.page.order.admin";
	private final String ERROR_PAGE = "path.page.error";
	
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("TakeOrdersCommand.execute()");
		
		String page = null;
		String status = (String) request.getParameter(STATUS);
		
		try {
			OrderDAO orderDAO = OrderDAOImpl.getInstance();
			
			if (status.equals("0")) {
				Client client = (Client) request.getSession().getAttribute(CLIENT);
				ArrayList<Order> orders = orderDAO.takeOrderByLogin(client.getLogin(), client);
				if (orders.isEmpty()) {
					request.setAttribute(MESSAGE, MessageManager.NO_ORDERS_ERROR);
				}
				request.getSession().setAttribute(ORDERS, orders);
				page = ConfigurationManager.getProperty(ORDER_CLIENT_PAGE);
				
			} else if (status.equals("1")) {
				List<Order> orders = orderDAO.findAll();
				if (orders.isEmpty()) {
					request.setAttribute(MESSAGE, MessageManager.NO_ORDERS_ERROR);
				}
				request.getSession().setAttribute(ORDERS, orders);	
				page = ConfigurationManager.getProperty(ORDER_ADMIN_PAGE);
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}
}
