<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<%@ include file="/jsp/jspf/bundle.jspf"%>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
    	<fmt:message key="title.main" />
    </title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" >
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
  </head>

  <body>
    <!-- Navigation for user-->
	<%@include file="/jsp/jspf/main_nav.jspf"%>
	<!--Custom tag-->
	<div class="container">
		<h5 align="center" id="message">
			<ctg:info name="${sessionScope.client.name}" surname="${sessionScope.client.surname}" locale="${sessionScope.locale}"/>
		</h5>
	</div>
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->

					<h2 id="hhh">
							<fmt:message key="info.client.login"/> ${sessionScope.client.login}<br/> 
							<fmt:message key="info.client.status"/> ${sessionScope.client.status}<br/>
							<fmt:message key="info.client.statusBL"/> ${sessionScope.client.statusBL}<br/>
					</h2>

						<div class="container-main-info">
							<form id="mainform-2" action="Controller" method="post">						
								<input type="hidden" name="command" value="to_basket" />
								<input class="btn btn-lg btn-default btn-block" type="submit" name="to_basket" value="<fmt:message key="button.to_basket" />" />
							</form>
							
							<form id="mainform" action="Controller" method="post">		
								<input type="hidden" name="command" value="to_order" />
								<input type="hidden" name="status" value="${sessionScope.client.status}" />
								<input class="btn btn-lg btn-default btn-block" type="submit" name="to_order" value="<fmt:message key="button.to_order" />" />
							</form>
						
						<c:if test="${sessionScope.client.status == 1}">
						
							<form id="mainform" action="Controller" method="post">						
								<input type="hidden" name="command" value="to_add_item" />
								<input class="btn btn-lg btn-default btn-block" type="submit" name="to_add_item" value="<fmt:message key="button.add_item" />" />
							</form>
							
							<form id="mainform" action="Controller" method="post">		
								<input type="hidden" name="command" value="take_clients" />
								<input class="btn btn-lg btn-default btn-block" type="submit" name="take_clients" value="<fmt:message key="button.take_clients" />" />
							</form>
							
						</c:if>
						</div>

			<div class="container" id="cont-main">
				<div class="row" id="main-list">
                    <div class="col-xs-6">
                        <div class="thumbnail" id="pic-border">
                           <img src="${pageContext.request.contextPath}/images/fon-4.jpg" alt=""/>
                            <div class="caption">
                                <h3><fmt:message key="static.logging" /></h3>
								<form action="Controller" method="post">		
									<input type="hidden" name="command" value="take_items_by_type" />
									<input type="hidden" name="item_type" value="1" />
									<input class="btn btn-md btn-default " type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
								</form>
                            </div>
                        </div>    
                    </div> 		
					<div class="col-xs-6">
                        <div class="thumbnail" id="pic-border">
                            <img src="${pageContext.request.contextPath}/images/fon-3.jpg" alt=""/>
                            <div class="caption">
                                <h3><fmt:message key="static.casualSummer" /></h3>
								<form action="Controller" method="post">		
									<input type="hidden" name="command" value="take_items_by_type" />
									<input type="hidden" name="item_type" value="2" />
									<input class="btn btn-md btn-default " type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
								</form>
                            </div>
                        </div>    
                    </div>
				</div>
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="thumbnail" id="pic-border">
                            <img src="${pageContext.request.contextPath}/images/fon-6.jpg" alt=""/>
                            <div class="caption" >
                                <h3><fmt:message key="static.basketball" /></h3>
								<form action="Controller" method="post">		
									<input type="hidden" name="command" value="take_items_by_type" />
									<input type="hidden" name="item_type" value="3" />
									<input class="btn btn-md btn-default " type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
								</form>
                            </div>
                        </div>    
                    </div> 		
					<div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="thumbnail" id="pic-border">
                            <img src="${pageContext.request.contextPath}/images/fon-1.jpg" alt=""/>
                            <div class="caption">
                                <h3><fmt:message key="static.football" /></h3>
								<form action="Controller" method="post">		
									<input type="hidden" name="command" value="take_items_by_type" />
									<input type="hidden" name="item_type" value="4" />
									<input class="btn btn-md btn-default " type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
								</form>
                            </div>
                        </div>    
                    </div>
					<div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="thumbnail" id="pic-border">
                            <img src="${pageContext.request.contextPath}/images/fon-2.jpg" alt=""/>
                            <div class="caption">
                                <h3><fmt:message key="static.casualWinter" /></h3>
								<form action="Controller" method="post">		
									<input type="hidden" name="command" value="take_items_by_type" />
									<input type="hidden" name="item_type" value="5" />
									<input class="btn btn-md btn-default " type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
								</form>
                            </div>
                        </div>    
                    </div> 		
					<div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="thumbnail" id="pic-border">
                            <img src="${pageContext.request.contextPath}/images/fon-5.jpeg" alt=""/>
                            <div class="caption">
                                <h3><fmt:message key="static.children" /></h3>
								<form action="Controller" method="post">		
									<input type="hidden" name="command" value="take_items_by_type" />
									<input type="hidden" name="item_type" value="6" />
									<input class="btn btn-md btn-default " type="submit" name="take_items" value="<fmt:message key="static.more"/>" />
								</form>
                            </div>
                        </div>    
                    </div>    
                </div>
			</div>
	
	
	 
    <%@include file="/jsp/jspf/footer.jspf"%>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
	
	</body>
	</html>