package by.epam.myshop.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import by.epam.myshop.entity.Client;
import by.epam.myshop.entity.Order;
import by.epam.myshop.exception.DAOException;

public interface OrderDAO extends GenericDAO<Integer, Order> {
	
	public List<Order> findAll() throws DAOException;
	public Order findEntityById(Integer id) throws DAOException;
	public boolean delete(Integer id) throws DAOException;
	public boolean create(Order client) throws DAOException;
	public ArrayList<Order> takeOrderByLogin(String login, Client client) throws DAOException;
	public boolean findOrdersByItemId(int itemId) throws DAOException;
	public boolean deleteItemsFromOrders(int orderId) throws DAOException;
	public boolean getIOAmountAndItemId(int orderId, HashMap<Integer, Integer> amountMap) throws DAOException;
	public boolean addItemsToOrder(Order order) throws DAOException;
	public boolean updateOrderStatus(int orderId, int status) throws DAOException;
	public boolean isExist(int id) throws DAOException;
	
}
