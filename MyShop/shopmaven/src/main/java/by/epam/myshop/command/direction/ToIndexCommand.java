package by.epam.myshop.command.direction;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.resource.ConfigurationManager;

public class ToIndexCommand implements ActionCommand {
	private static Logger LOG = Logger.getLogger(ToIndexCommand.class);
	private final String URL = "url";
	private final String INDEX_PAGE = "path.page.index";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("ToIndexCommand.execute()");
		
		String page = ConfigurationManager.getProperty(INDEX_PAGE);
		request.getSession().setAttribute(URL, page);
		return page;
	}
}