<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>

<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.items"/></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
    <script src="js/ie-emulation-modes-warning.js"></script>
	
  </head>

  <body>
	<!-- Navigation for user-->
	<%@include file="/jsp/jspf/ind_nav.jspf"%>
	<!---->
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->

		<div class="container">
                    
            <div class="row">
				<c:forEach var="item" items="${items}">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="thumbnail" id="border">
                            <img src="${item.itemPicPath}" alt="" width="280" height="280"/>
                            <div class="caption" id="margin" >
                                <h3 id="font"><fmt:message key="info.item.name"/>  ${item.itemName}</h3>
								<h3 id="font"><fmt:message key="info.item.id"/> ${item.itemId}</h3>
								<h3 id="font"><fmt:message key="info.item.amount_shop"/> ${item.itemsAmount}</h3>
								<h3 id="font"><fmt:message key="info.item.manufacter"/> ${item.manufacterName}</h3>
	                            <h3 id="font"><fmt:message key="info.item.desc"/> ${item.itemDescription}</p>
								
								<form action="Controller" method="post">
										<div class="form-group form-group-sm">
											<label for="amount"><h3 id="font"><fmt:message key="info.item.amount"/></h3></label>
											<select class="form-control" id="amount" name="amount" >
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
											</select>
										</div>

										<input class="btn btn-block btn-default" type="submit" value="<fmt:message key="button.item.add_to_order" />" /> 

										<input type="hidden" name="item_id" value="${item.itemId}" />
										<input type="hidden" name="client" value="${sessionScope.client}" /> 
										<input type="hidden" name="items" value="${items}" /> 
										<input type="hidden" name="basket" value="${sessionScope.basket}" /> 
										<input type="hidden" name="amount_map" value="${sessionScope.amount_map}" />
										<input type="hidden" name="command" value="add_item_to_basket" /> 
								</form>

								<c:if test="${sessionScope.client.status == 1}">
									<form action="Controller" method="post">
										<input class="btn btn-block btn-default" type="submit" value="<fmt:message key="button.item.modify" />" /> 
										<input type="hidden" name="item_id" value="${item.itemId}" />
										<input type="hidden" name="command" value="to_modify_item" />
									</form>
									<form action="Controller" method="post">
										<input class="btn btn-block btn-default" type="submit" value="<fmt:message key="button.item.delete" />" /> 
										<input type="hidden" name="command" value="delete_item" />
										<input type="hidden" name="item_id" value="${item.itemId}" />
									</form>
								</c:if>

							</div> 
                        </div>       
                    </div> 
				</c:forEach>					
                    
            </div>   
     </div>



	 <%@include file="/jsp/jspf/footer.jspf"%>
  </body>
</html>