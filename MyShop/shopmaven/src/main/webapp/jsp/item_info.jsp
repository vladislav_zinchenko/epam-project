<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>

<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.items"/></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
    <script src="js/ie-emulation-modes-warning.js"></script>
	
  </head>

  <body>
	<!-- Navigation for user-->
	<%@include file="/jsp/jspf/ind_nav.jspf"%>
	<!---->
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->

	<div class="container">
		<div class="thumbnail">
                        <img src="http://placehold.it/300x240" alt=""/>
                            <div class="caption">
                                <h3>${item.itemName}</h3>
								<h3>${item.itemId}</h3>
								<h3>${item.itemsAmount}</h3>
								<h3>${item.itemType}</h3>
								<h3>${item.itemPicPath}</h3>
								<h3>${item.manufacterName}</h3>
                                <p>${item.itemDescription}</p>
							</div> 
								
							<form action="Controller" method="post">
								<input class="btn btn-default" type="submit" value="<fmt:message key="button.item.add_to_order" />" /> 
								<!--<input type="text" class="form-control" name="amount" value="1">-->
										<div class="form-group form-group-sm">
											<label for="amount">Amount</label>
											<select class="form-control" id="amount" name="amount">
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											</select>
										</div>
									<input type="hidden" name="item_id" value="${item.itemId}" />
									<input type="hidden" name="client" value="${sessionScope.client}" /> 
									<input type="hidden" name="items" value="${items}" /> 
									<input type="hidden" name="basket" value="${sessionScope.basket}" /> 
									<input type="hidden" name="amount" value="${sessionScope.amount_map}" />
									<!--<input type="hidden" name="amount" value="${item.itemsAmount}" />-->
									<input type="hidden" name="command" value="add_item_to_basket" /> 
							</form>
					
					
							<c:if test="${sessionScope.client.status == 1}">
								<form action="Controller" method="post">
									<input class="btn btn-default" type="submit" value="<fmt:message key="button.item.modify" />" /> 
									<input type="hidden" name="item_id" value="${item.itemId}" />
									<input type="hidden" name="command" value="to_modify_item" />
								</form>
								<form action="Controller" method="post">
									<input class="btn btn-default" type="submit" value="<fmt:message key="button.item.delete" />" /> 
									<input type="hidden" name="command" value="delete_item" />
									<input type="hidden" name="item_id" value="${item.itemId}" />
								</form>
							</c:if>    
                        </div>


	</div>
	
	
		

	 <%@include file="/jsp/jspf/footer.jspf"%>
  </body>
</html>