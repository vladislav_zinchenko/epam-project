<%@ page language="java" contentType="text/html" errorPage="error" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.main" /></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" >
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
  </head>
<body>
	<!-- Navigation for user-->
	<%@include file="/jsp/jspf/ind_nav.jspf"%>
	<!---->

	<div class="container-main-info">
		<c:choose>
			<c:when test="${empty message}">
				<h3>
					<fmt:message key="error.goback" />
				</h3>
			</c:when>
			<c:otherwise>
				<h3>
					<fmt:message key="${message}" />
				</h3>
			</c:otherwise>
		</c:choose>
	</div>
	
	<%@include file="/jsp/jspf/footer.jspf"%>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>
</html>