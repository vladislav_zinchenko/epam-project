package by.epam.myshop.dao.pool;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import by.epam.myshop.dao.resource.DBParameter;
import by.epam.myshop.dao.resource.DBResourceManager;
import by.epam.myshop.exception.ConnectionPoolException;

public class DBConnectionPool {
	
	private static final Logger LOG = Logger.getLogger(DBConnectionPool.class);
    private static final String DRIVER = DBResourceManager.getValue(DBParameter.DB_DRIVER);
    private static final String URL = DBResourceManager.getValue(DBParameter.DB_URL);
    private static final String LOGIN = DBResourceManager.getValue(DBParameter.DB_USER);
    private static final String PASS = DBResourceManager.getValue(DBParameter.DB_PASSWORD);
    private static final int SIZE = Integer.parseInt(DBResourceManager.getValue(DBParameter.DB_POOL_SIZE));
    private static final String ERROR_INFO = "Connection not in the usedConnections";
    private BlockingQueue<ConnectionWrapper> availableConnections;
    private BlockingQueue<ConnectionWrapper> usedConnections;
    private static DBConnectionPool instance;
    private static final Lock LOCK = new ReentrantLock();
    private String url;
    private String password;
    private String user;

    public void initializeAvailableConnection() throws ConnectionPoolException {
    	LOG.info("initializeAvailableConnection()");
    	LOG.info("av.con. before init:" + availableConnections.size());
    	LOG.info("used.con. before init:" + usedConnections.size());
    	
        for (int i = 0; i < SIZE; i++) {
            Connection connection = getConnection();
            ConnectionWrapper connectionWrapper = new ConnectionWrapper(connection);
            availableConnections.add(connectionWrapper);
        }
        
        LOG.info("av.con. after init:" + availableConnections.size());
    	LOG.info("used.con. after init:" + usedConnections.size());
    }

        private Connection getConnection() throws ConnectionPoolException {
        Connection connection;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new ConnectionPoolException(e);
        }
        return connection;
    }

    private DBConnectionPool(String url, String user, String password) {
    	LOG.info("DBConnectionPool()");
        try {
            Class.forName(DRIVER);
            this.url = url;
            this.password = password;
            this.user = user;
            availableConnections = new ArrayBlockingQueue<>(SIZE);
            usedConnections = new ArrayBlockingQueue<>(SIZE);
        } catch (Exception e) {
            LOG.error(e);
        }
    }

    public static DBConnectionPool getInstance() {
    	LOG.info("DBConnectionPool.getIntance()");
        try {
            LOCK.lock();
            if (instance == null) {
                instance = new DBConnectionPool(URL, LOGIN, PASS);
            }
        } finally {
            LOCK.unlock();
        }
        return instance;
    }

    
    public Connection takeConnection() throws ConnectionPoolException {
    	LOG.info("DBConnectionPool.takeConnection()");
        ConnectionWrapper newConnection;
        try {
            newConnection = availableConnections.take();
            usedConnections.put(newConnection);
        } catch (InterruptedException e) {
            throw new ConnectionPoolException(e);
        }
        LOG.info("av.con. after take:" + availableConnections.size());
    	LOG.info("used.con. after take:" + usedConnections.size());
        return newConnection;
    }

    public void closeConnection(Connection c) throws ConnectionPoolException {
    	LOG.info("DBConnectionPool.closeConnection()");
    	
    	LOG.info("Connection to close:" + c);
    	
    	LOG.info("Is contain:" + usedConnections.contains(c));
    	
        try {
            if (c != null) {
                if (usedConnections.remove(c)) {
                    availableConnections.put((ConnectionWrapper) c);
                } else {
                    throw new ConnectionPoolException("Connection isn't contained in the usedConnections");
                }
            }
        } catch (InterruptedException e) {
            throw new ConnectionPoolException(e);
        }
        LOG.info("av.con. after closeConnection:" + availableConnections.size());
    	LOG.info("used.con. after closeConnection:" + usedConnections.size());
    }

    public void destroyConnectionPool() throws ConnectionPoolException {
    	LOG.info("DBConnectionPool.destroyConnectionPool()");
    	LOG.info("av.con. before destroy:" + availableConnections.size());
    	LOG.info("used.con. before destroy:" + usedConnections.size());
        try {
            closeConnectionQueue(availableConnections);
        } catch (ConnectionPoolException e) {
            throw new ConnectionPoolException("Can't correctly destroy connection pool.");
        } finally {
            closeConnectionQueue(usedConnections);
        }
        LOG.info("av.con. after destroy:" + availableConnections.size());
    	LOG.info("used.con. after destroy:" + usedConnections.size());
    }

    private void closeConnectionQueue(BlockingQueue<ConnectionWrapper> queue) throws ConnectionPoolException {
    	LOG.info("DBConnectionPool.closeConnectionQueue()");
        boolean errorStatus = false;
        for (Connection connection : queue) {
            try {
                if (connection != null) { 
                    ((ConnectionWrapper) connection).dispose();
                }
            } catch (SQLException e) {
                errorStatus = true;
            }
        }
        if (errorStatus) {
            throw new ConnectionPoolException("Can't close connection queue.");
        }
    }

    private class ConnectionWrapper implements Connection {

        private Connection connection;

        public ConnectionWrapper(Connection connection) {
            this.connection = connection;
        }

        @Override
        public Statement createStatement() throws SQLException {
            return connection.createStatement();
        }

        @Override
        public PreparedStatement prepareStatement(String sql) throws SQLException {
            return connection.prepareStatement(sql);
        }

        @Override
        public CallableStatement prepareCall(String sql) throws SQLException {
            return connection.prepareCall(sql);
        }

        @Override
        public String nativeSQL(String sql) throws SQLException {
            return connection.nativeSQL(sql);
        }

        @Override
        public void setAutoCommit(boolean autoCommit) throws SQLException {
            connection.setAutoCommit(autoCommit);
        }

        @Override
        public boolean getAutoCommit() throws SQLException {
            return connection.getAutoCommit();
        }

        @Override
        public void commit() throws SQLException {
            connection.commit();
        }

        @Override
        public void rollback() throws SQLException {
            connection.rollback();
        }

        @Override
        public void close() throws SQLException {
        	LOG.info("WrapperConnection.close()");
        	LOG.info("Connection in close():" + connection);
        	
            if (connection.isClosed()) {
                throw new SQLException("Can't close closed exception.");
            }
            try {
                closeConnection(connection);
            } catch (ConnectionPoolException e) {
                LOG.error(e);
            }
        }

        private void dispose() throws SQLException {
        	LOG.info("WrapperConnection.dispose()");
            this.connection.close();
        }

        @Override
        public boolean isClosed() throws SQLException {
            return connection.isClosed();
        }

        @Override
        public DatabaseMetaData getMetaData() throws SQLException {
            return connection.getMetaData();
        }

        @Override
        public void setReadOnly(boolean readOnly) throws SQLException {
            connection.setReadOnly(readOnly);
        }

        @Override
        public boolean isReadOnly() throws SQLException {
            return connection.isReadOnly();
        }

        @Override
        public void setCatalog(String catalog) throws SQLException {
            connection.setCatalog(catalog);
        }

        @Override
        public String getCatalog() throws SQLException {
            return connection.getCatalog();
        }

        @Override
        public void setTransactionIsolation(int level) throws SQLException {
            connection.setTransactionIsolation(level);
        }

        @Override
        public int getTransactionIsolation() throws SQLException {
            return connection.getTransactionIsolation();
        }

        @Override
        public SQLWarning getWarnings() throws SQLException {
            return connection.getWarnings();
        }

        @Override
        public void clearWarnings() throws SQLException {
            connection.clearWarnings();
        }

        @Override
        public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
        }

        @Override
        public Map<String, Class<?>> getTypeMap() throws SQLException {
            return connection.getTypeMap();
        }

        @Override
        public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
            connection.setTypeMap(map);
        }

        @Override
        public void setHoldability(int holdability) throws SQLException {
            connection.setHoldability(holdability);
        }

        @Override
        public int getHoldability() throws SQLException {
            return connection.getHoldability();
        }

        @Override
        public Savepoint setSavepoint() throws SQLException {
            return connection.setSavepoint();
        }

        @Override
        public Savepoint setSavepoint(String name) throws SQLException {
            return connection.setSavepoint(name);
        }

        @Override
        public void rollback(Savepoint savepoint) throws SQLException {
            connection.rollback();
        }

        @Override
        public void releaseSavepoint(Savepoint savepoint) throws SQLException {
            connection.releaseSavepoint(savepoint);
        }

        @Override
        public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
            return connection.prepareStatement(sql, autoGeneratedKeys);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
            return connection.prepareStatement(sql, columnIndexes);
        }

        @Override
        public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
            return connection.prepareStatement(sql, columnNames);
        }

        @Override
        public Clob createClob() throws SQLException {
            return connection.createClob();
        }

        @Override
        public Blob createBlob() throws SQLException {
            return connection.createBlob();
        }

        @Override
        public NClob createNClob() throws SQLException {
            return connection.createNClob();
        }

        @Override
        public SQLXML createSQLXML() throws SQLException {
            return connection.createSQLXML();
        }

        @Override
        public boolean isValid(int timeout) throws SQLException {
            return connection.isValid(timeout);
        }

        @Override
        public void setClientInfo(String name, String value) throws SQLClientInfoException {
            connection.setClientInfo(name, value);
        }

        @Override
        public void setClientInfo(Properties properties) throws SQLClientInfoException {
            connection.setClientInfo(properties);
        }

        @Override
        public String getClientInfo(String name) throws SQLException {
            return connection.getClientInfo(name);
        }

        @Override
        public Properties getClientInfo() throws SQLException {
            return connection.getClientInfo();
        }

        @Override
        public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
            return connection.createArrayOf(typeName, elements);
        }

        @Override
        public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
            return connection.createStruct(typeName, attributes);
        }

        @Override
        public void setSchema(String schema) throws SQLException {
            connection.setSchema(schema);
        }

        @Override
        public String getSchema() throws SQLException {
            return connection.getSchema();
        }

        @Override
        public void abort(Executor executor) throws SQLException {
            connection.abort(executor);
        }

        @Override
        public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
            connection.setNetworkTimeout(executor, milliseconds);
        }

        @Override
        public int getNetworkTimeout() throws SQLException {
            return connection.getNetworkTimeout();
        }

        @Override
        public <T> T unwrap(Class<T> iface) throws SQLException {
            return connection.unwrap(iface);
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return connection.isWrapperFor(iface);
        }
    }
	
}
