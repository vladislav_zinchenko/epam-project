package by.epam.myshop.util;

import java.util.regex.Pattern;

import by.epam.myshop.entity.Client;
import by.epam.myshop.entity.Item;
import by.epam.myshop.resource.MessageManager;

public class Validator {
	
	private static final Pattern LOGIN = Pattern.compile("[\\w]{1,30}");
	private static final Pattern PASSWORD = Pattern.compile("[\\w]{3,30}");
	private static final Pattern NAME_SURNAME = Pattern.compile("[�-��-�A-Za-z]{2,30}");
	private static final Pattern AMOUNT = Pattern.compile("[1-9][0-9]{0,1}");
	private static final Pattern ITEM_NAME = Pattern.compile("[�-�A-Z�-�a-z\\s- 0-9]{1,45}");
	private static final Pattern DESCRIPTION = Pattern.compile("[�-�A-Z�-�a-z\\s-]{2,45}");
	private static final Pattern PATH = Pattern.compile("[�-�A-Z�-�a-z0-9\\s-\\:./]{2,100}");
	private static final Pattern MANUFACTER = Pattern.compile("[�-�A-Z�-�a-z\\s]{2,45}");
	
	public static String checkClientInfo(Client client) {
		if (!LOGIN.matcher(client.getLogin()).matches()) {
			return MessageManager.INCORRECT_LOGIN;
		}
		if (!PASSWORD.matcher(client.getPassword()).matches()) {
			return MessageManager.INCORRECT_PASSWORD;
		}
		if (!NAME_SURNAME.matcher(client.getName()).matches() && !NAME_SURNAME.matcher(client.getSurname()).matches()) {
			return MessageManager.INCORRECT_NAME_SURNAME;
		}
		return null;
	}
	
	public static String checkItemInfo(Item item) {
		if (!ITEM_NAME.matcher(item.getItemName()).matches()) {
			return MessageManager.INCORRECT_NAME_ITEM;
		}
		if (!AMOUNT.matcher(String.valueOf(item.getItemsAmount())).matches()) {
			return MessageManager.INCORRECT_AMOUNT;
		}
		if (!DESCRIPTION.matcher(item.getItemDescription()).matches()) {
			return MessageManager.INCORRECT_DESC;
		}
		if (!PATH.matcher(item.getItemPicPath()).matches()) {
			return MessageManager.INCORRECT_PATH;
		}
		if (!MANUFACTER.matcher(item.getManufacterName()).matches()) {
			return MessageManager.INCORRECT_MANUFACTER;
		}
		return null;
	}
	
	public static boolean checkEmptyField(String... args) {
		if (args.length == 0) {
			return false;
		} else {
			for (String arg : args) {
				if (arg.length() == 0) {
					return false;
				}
			}
		}
		return true;
	}
}
