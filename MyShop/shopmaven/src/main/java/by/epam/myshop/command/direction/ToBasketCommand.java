package by.epam.myshop.command.direction;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.resource.ConfigurationManager;

public class ToBasketCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(ToBasketCommand.class);
	private final String URL = "url";
	private final String BASKET_PAGE = "path.page.basket";

	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("ToBasketCommand.execute()");
		String page = null;
		page = ConfigurationManager.getProperty(BASKET_PAGE);
		request.getSession().setAttribute(URL, page);
		return page;
	}
}
