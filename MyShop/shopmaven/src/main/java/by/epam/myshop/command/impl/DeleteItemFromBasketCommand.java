package by.epam.myshop.command.impl;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ItemDAO;
import by.epam.myshop.dao.impl.ItemDAOImpl;
import by.epam.myshop.entity.Item;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class DeleteItemFromBasketCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(DeleteItemFromBasketCommand.class);
	private final String BASKET = "basket";
	private final String AMOUNT = "amount";
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String ITEM_ID = "item_id";
	private final String AMOUNT_MAP = "amount_map";
	private final String ERROR_PAGE = "path.page.error";
	private final String BASKET_PAGE = "path.page.basket";

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		
		String page = null;
		int itemId = Integer.parseInt(request.getParameter(ITEM_ID));
		List<Item> basket = (List<Item>) request.getSession().getAttribute(BASKET);
		HashMap<Integer, Integer> amountMap = (HashMap<Integer, Integer>) request.getSession().getAttribute(AMOUNT_MAP);

		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		try {
			for (Item item : basket) {
				if (item.getItemId() == itemId) {
					basket.remove(item);
					item.setItemsAmount(item.getItemsAmount() + amountMap.get(item.getItemId()));
					itemDAO.modifyItemInfo(item);
					amountMap.remove(item.getItemId());
					request.getSession().setAttribute(AMOUNT_MAP, amountMap);
					request.setAttribute(AMOUNT, item.getItemsAmount());
					request.setAttribute(MESSAGE, MessageManager.ITEM_DELETED_FROM_BASKET_SUCCESSFULLY);
					break;
				}
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(BASKET, basket);
		page = ConfigurationManager.getProperty(BASKET_PAGE);
		request.getSession().setAttribute(URL, page);
		return page;
	}
}
