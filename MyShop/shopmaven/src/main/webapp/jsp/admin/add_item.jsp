<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Main page</title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" >
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/css/admin.css" rel="stylesheet" type="text/css">
	
  </head>

  <body>
  
    <!-- Navigation for user-->
	<%@include file="/jsp/jspf/ind_nav.jspf"%>
	<!---->
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->


	<div class="container-main-info" padding="20px" id="border">
	<form id="modifyform"  method="POST" action="Controller">
		<div class="form-group">
			<label for="name" id="font"><fmt:message key="static.add.item.name"/></label>
			<input type="text" class="form-control" id="name" name="name" pattern="[А-ЯA-Zа-яa-z\\s- 0-9]{1,45}" required>
		</div>
		<div class="form-group">
			<label for="type" id="font"><fmt:message key="static.add.item.type"/></label>
			<select class="form-control" id="type" name="type" required>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
			</select>
		</div>
		<div class="form-group">
			<label for="amount" id="font"><fmt:message key="static.add.item.amount"/></label>
			<input type="text" class="form-control" id="amount" name="amount" pattern="[1-9][0-9]{0,1}" required>
		</div>
		<div class="form-group">
			<label for="desc" id="font"><fmt:message key="static.add.item.desc"/></label>
			<textarea class="form-control" rows="3"	id="desc" name="description" pattern="[А-ЯA-Zа-яa-z\\s-]{2,45}" required></textarea>
		</div>
		<div class="form-group">
			<label for="path" id="font"><fmt:message key="static.add.item.pic"/></label>
			<input type="text" class="form-control" id="path" name="path" pattern="[А-ЯA-Zа-яa-z\\s-\\:/]{2,45}" required>
		</div>
		<div class="form-group">
			<label for="manufacter" id="font"><fmt:message key="static.add.item.manufacter"/></label>
			<input type="text" class="form-control" id="manufacter" name="manufacter" pattern="[А-ЯA-Zа-яa-z\\s]{2,45}" required> 
		</div>
		
		<input type="hidden" name="command" value="add_item"/>
		<input class="btn btn-lg btn-default btn-block" type="submit" value="<fmt:message key="button.add_item"/> "/>
		
	</form>
	</div>
	
	
	 <%@include file="/jsp/jspf/footer.jspf"%>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
	
	</body>
</html>