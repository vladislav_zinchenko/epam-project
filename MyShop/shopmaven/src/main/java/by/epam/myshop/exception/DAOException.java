package by.epam.myshop.exception;

@SuppressWarnings("serial")
public class DAOException extends Exception{

	public DAOException() {
		super();
	}

	public DAOException(String msg, Exception e) {
		super(msg, e);
	}

	public DAOException(String msg) {
		super(msg);
	}

	public DAOException(Exception e) {
		super(e);
	}
}
