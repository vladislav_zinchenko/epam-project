package by.epam.myshop.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import by.epam.myshop.dao.pool.DBConnectionPool;
import by.epam.myshop.exception.ConnectionPoolException;

public class ShopServletContextListener implements ServletContextListener {
	private static final Logger LOG = Logger.getLogger(ShopServletContextListener.class);
	@Override
	public void contextInitialized(ServletContextEvent event) {
		LOG.info("ShopServletContextListener.contextInit()");
		try {
			DBConnectionPool.getInstance().initializeAvailableConnection();
		} catch (ConnectionPoolException e) {
			throw new RuntimeException("Error while initializing connection pool", e);
		}
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		LOG.info("ShopServletContextListener.contextDestroyed()");
		try {
			DBConnectionPool.getInstance().destroyConnectionPool();
		} catch (ConnectionPoolException e) {
			LOG.error(e);
		}
	}

}
