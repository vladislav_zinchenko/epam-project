package by.epam.myshop.command.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ItemDAO;
import by.epam.myshop.dao.impl.ItemDAOImpl;
import by.epam.myshop.entity.Item;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class TakeItemsCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(TakeItemsCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String ITEMS = "items";
	private final String ITEMS_PAGE = "path.page.items";
	private final String ERROR_PAGE = "path.page.error";
	
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("TakeItemsCommand.execute()");
		
		String page = null;
		ItemDAO itemDAO = ItemDAOImpl.getInstance();
		try {
			List<Item> items = itemDAO.findAll();
			if (items.isEmpty()) {
				request.setAttribute(MESSAGE, MessageManager.SEARCHING_ITEM_ERROR);
				page = ConfigurationManager.getProperty(ERROR_PAGE); 
			} else {
				request.getSession().setAttribute(ITEMS, items);
				page = ConfigurationManager.getProperty(ITEMS_PAGE); 
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;	
	}
}
