package by.epam.myshop.exception;

@SuppressWarnings("serial")
public class ConnectionPoolException extends Exception {
	
	public ConnectionPoolException() {
		super();
	}

	public ConnectionPoolException(String msg, Exception e) {
		super(msg, e);
	}

	public ConnectionPoolException(String msg) {
		super(msg);
	}

	public ConnectionPoolException(Exception e) {
		super(e);
	}
}
