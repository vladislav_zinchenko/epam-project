package by.epam.myshop.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.myshop.dao.OrderDAO;
import by.epam.myshop.dao.pool.DBConnectionPool;
import by.epam.myshop.entity.Client;
import by.epam.myshop.entity.Item;
import by.epam.myshop.entity.Order;
import by.epam.myshop.exception.ConnectionPoolException;
import by.epam.myshop.exception.DAOException;

public class OrderDAOImpl implements OrderDAO {

private static final Logger LOG = Logger.getLogger(OrderDAOImpl.class);
	
	private static OrderDAOImpl instance = new OrderDAOImpl();
	
	private static final String OR_ORDER_ID = "order.order_id";
	private static final String ORDER_STATUS = "order.status";
	private static final String ORDER_DATE = "order.date";
	private static final String ORDER_CLIENT_LOGIN = "order.client_login";
	private static final String ITEM_ID = "item_id";
	private static final String NAME = "name";
	private static final String AMOUNT = "amount";
	private static final String MANUFACTER = "manufacter";
	private static final String DESC = "description";
	private static final String PATH = "path";
	private static final String TYPE = "type";
	private static final String IO_AMOUNT = "io_amount";
	private static final String ORDER_ID = "order_id";
	
	private static final String SQL_SELECT_ORDERS = "SELECT * FROM shop.order JOIN shop.item_order ON (order.order_id = item_order.order_id) JOIN shop.item ON (item_order.item_id = item.item_id)";
	private static final String SQL_FIND_ORDER_BY_LOGIN = "SELECT * FROM shop.order JOIN shop.item_order ON (order.order_id = item_order.order_id) JOIN shop.item ON (item_order.item_id = item.item_id) WHERE client_login=? ORDER BY order.date DESC";
	private static final String SQL_CREATE_ORDER = "INSERT INTO shop.order (order_id, date, client_login, status) VALUES (?,?,?,?)";
	private static final String SQL_ADD_ITEM_TO_ORDER = "INSERT INTO shop.item_order (order_id, item_id, io_amount) VALUES (?,?,?)";
	private static final String SQL_DELETE_ORDER = "DELETE FROM shop.order WHERE order_id = ?";
	private static final String SQL_DELETE_ITEMS_FROM_ORDERS = "DELETE FROM shop.item_order WHERE order_id = ?";
	private static final String SQL_SELECT_IOAMOUNT_AND_ITEM_ID_FROM_ORDERS = "SELECT io_amount, item_id FROM shop.item_order WHERE order_id = ?";
	private static final String SQL_UPDATE_ORDER_STATUS = "UPDATE shop.order SET status= ? WHERE order_id= ?";
	private static final String SQL_SELECT_ORDER_BY_ITEM_ID = "SELECT * FROM shop.item_order where item_id = ?";
	private static final String SQL_SELECT_ORDER_BY_ID = "SELECT * FROM shop.order WHERE order_id = ?";


	private OrderDAOImpl() {
		super();
	}

	public static OrderDAO getInstance() {
		LOG.info("OrderDAOImpl.getInstance");
		return instance;
	}

	@Override
	public ArrayList<Order> findAll() throws DAOException {
		LOG.info("OrderDAO.takeOrders()");
		
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		ArrayList<Order> orders = new ArrayList<>();
		Order order = null;
		Client client = new Client();
		ArrayList<Item> items = new ArrayList<>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		HashMap<Integer, Integer> ioAmountMap = new HashMap<>();
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_SELECT_ORDERS);
			ResultSet resultSet = prStatement.executeQuery();
			int flag = 0;
			while (resultSet.next()) {
				flag = resultSet.getInt(OR_ORDER_ID);

				if (order != null && order.getOrderId() != flag && order.getItems() != null) {
					orders.add(order);
					order = new Order();
				}

				if (order == null || order.getOrderId() != flag) { // ���� true � ������ �����, �� ������ �� ����������, �.�. null.getOrderId() �� �����
					order = new Order();
					client = new Client();
					order.setOrderId(flag);
					order.setStatus(resultSet.getInt(ORDER_STATUS));
					order.setDate(resultSet.getString(ORDER_DATE));
					client.setLogin(resultSet.getString(ORDER_CLIENT_LOGIN));
					items = new ArrayList<Item>();
				}
				
				if (order.getOrderId() == resultSet.getInt(ORDER_ID)) {
					Item item = new Item();
					item.setItemId(resultSet.getInt(ITEM_ID));
					item.setItemName(resultSet.getString(NAME));
					item.setItemsAmount(resultSet.getInt(AMOUNT));
//					item.setItemsAmount(amountMap.get(item.getItemId()));
					
					item.setManufacterName(resultSet.getString(MANUFACTER));
					item.setItemDescription(resultSet.getString(DESC));
					item.setItemPicPath(resultSet.getString(PATH));
					item.setItemType(resultSet.getString(TYPE));
					
					ioAmountMap.put(item.getItemId(), resultSet.getInt(IO_AMOUNT));
					LOG.info("io_amount:" + ioAmountMap);
					
					items.add(item);
					order.setItems(items);
					order.setClient(client);
				}
				order.setAmountMap(ioAmountMap);
			}
			if (order != null) {
				orders.add(order);
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return orders;
	}

	public ArrayList<Order> takeOrderByLogin(String login, Client client) throws DAOException {
		LOG.info("OrderDAO.takeOrderByLogin");
		
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		ArrayList<Order> orders = new ArrayList<>();
		Order order = null;
		ArrayList<Item> items = new ArrayList<>();
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_FIND_ORDER_BY_LOGIN);
			prStatement.setString(1, login);
			ResultSet resultSet = prStatement.executeQuery();
			int flag = 0;
			while (resultSet.next()) {
				flag = resultSet.getInt(ORDER_ID);

				if (order != null && order.getOrderId() != flag && order.getItems() != null) {
					orders.add(order);
				}

				if (order == null || order.getOrderId() != flag) {
					order = new Order();
					order.setOrderId(flag);
					order.setStatus(resultSet.getInt(ORDER_STATUS));
					order.setDate(resultSet.getString(ORDER_DATE));
					items = new ArrayList<Item>();

				}
				if (order.getOrderId() == resultSet.getInt(ORDER_ID)) {
					Item item = new Item();
					item.setItemId(resultSet.getInt(ITEM_ID));
					item.setItemName(resultSet.getString(NAME));
					item.setItemsAmount(resultSet.getInt(AMOUNT));
					item.setManufacterName(resultSet.getString(MANUFACTER));
					item.setItemDescription(resultSet.getString(DESC));
					item.setItemPicPath(resultSet.getString(PATH));
					item.setItemType(resultSet.getString(TYPE));
					
					items.add(item);
					order.setItems(items);
					order.setClient(client);
				}

			}
			if (order != null) {
				orders.add(order);
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return orders;
	}

	public boolean findOrdersByItemId(int itemId) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_SELECT_ORDER_BY_ITEM_ID);
			prStatement.setInt(1, itemId);
			ResultSet resultSet = prStatement.executeQuery();
			if (resultSet.next()) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}

	@Override
	public boolean delete(Integer orderId) throws DAOException {
		
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_DELETE_ORDER);
			prStatement.setInt(1, orderId);
			if (deleteItemsFromOrders(orderId)) {
				if (prStatement.executeUpdate() > 0) {
					result = true;
				}
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}

	public boolean deleteItemsFromOrders(int orderId) throws DAOException {
		LOG.info("OrderDAO.deleteItemsFromOrders()");
		
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();			
			prStatement = connection.prepareStatement(SQL_DELETE_ITEMS_FROM_ORDERS);
			prStatement.setInt(1, orderId);
			if (prStatement.executeUpdate() > 0) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}
	
	public boolean getIOAmountAndItemId(int orderId, HashMap<Integer, Integer> amountMap) throws DAOException {
		LOG.info("OrderDAO.getIOAmountAndItemId()");
		
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_SELECT_IOAMOUNT_AND_ITEM_ID_FROM_ORDERS);
			prStatement.setInt(1, orderId);
			ResultSet resultSet = prStatement.executeQuery();
			while (resultSet.next()) {
				amountMap.put(resultSet.getInt(ITEM_ID), resultSet.getInt(IO_AMOUNT));
			}
			result = true;
			LOG.info(amountMap);
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}
	
	@Override
	public boolean create(Order order) throws DAOException {
		LOG.info("OrderDAO.createOrder()");
		
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		GregorianCalendar gc = new GregorianCalendar();
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_CREATE_ORDER);
			prStatement.setInt(1, order.getOrderId());
			prStatement.setString(2, gc.getTime().toString());
			prStatement.setString(3, order.getClient().getLogin());
			prStatement.setInt(4, order.getStatus());
			
			if (prStatement.executeUpdate() > 0 && addItemsToOrder(order)) {
				result = true;
			}
			
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}

	public boolean addItemsToOrder(Order order) throws DAOException {
		LOG.info("OrderDAO.addItemsToOrder()");
		
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		int orderId = order.getOrderId();
		List<Item> items = order.getItems();
		HashMap<Integer, Integer> amountMap = order.getAmountMap();
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_ADD_ITEM_TO_ORDER);
			for (Item item : items) {
				result = false;
				prStatement.setInt(1, orderId);
				prStatement.setInt(2, item.getItemId());
				prStatement.setInt(3, amountMap.get(item.getItemId()));
				if (prStatement.executeUpdate() == 1) {
					result = true;
				}
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}
	
	public boolean updateOrderStatus(int orderId, int status) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_UPDATE_ORDER_STATUS);
			if (status == 0) {
				prStatement.setInt(1, 1);
			} else {
				prStatement.setInt(1, 0);
			}
			prStatement.setInt(2, orderId);
			if (prStatement.executeUpdate() == 1) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}
	
	@Override
	public boolean isExist(int orderId) throws DAOException {
		DBConnectionPool pool = DBConnectionPool.getInstance(); 
		
		boolean result = false;
		Connection connection = null;
		PreparedStatement prStatement = null;
		try {
			connection = pool.takeConnection();
			prStatement = connection.prepareStatement(SQL_SELECT_ORDER_BY_ID);
			prStatement.setInt(1, orderId);
			ResultSet resultSet = prStatement.executeQuery();
			if (resultSet.next()) {
				result = true;
			}
		} catch (SQLException | ConnectionPoolException e) {
			throw new DAOException(e);
		} finally {
			try {
				if (prStatement != null) {
                    prStatement.close();
                }
				pool.closeConnection(connection);
			} catch (SQLException | ConnectionPoolException e) {
	            throw new DAOException(e);
	        }		
		} 
		return result;
	}

	
	@Override
	public Order findEntityById(Integer id) throws DAOException {
		try {
	        throw new UnsupportedOperationException();
	    } catch (UnsupportedOperationException e) {
	    	LOG.error("UnsupportedOperationException", e);
	    }
		return null;
	}

	
}
