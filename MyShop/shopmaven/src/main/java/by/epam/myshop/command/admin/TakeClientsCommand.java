package by.epam.myshop.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ClientDAO;
import by.epam.myshop.dao.impl.ClientDAOImpl;
import by.epam.myshop.entity.Client;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class TakeClientsCommand implements ActionCommand {
	private static final Logger LOG = Logger.getLogger(TakeClientsCommand.class);
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String CLIENTS = "clients";
	private final String ERROR_PAGE = "path.page.error";
	private final String TAKE_CLIENTS_PAGE = "path.page.admin.take_clients";
	
	@Override
	public String execute(HttpServletRequest request) {
		String page;
		ClientDAO clientDAO = ClientDAOImpl.getInstance();
		try {
			List<Client> clients = clientDAO.findAll();
			LOG.info(clients.toString());
			if (clients.isEmpty()) {
				request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
				page =  ConfigurationManager.getProperty(ERROR_PAGE);
			} else {
				request.getSession().setAttribute(CLIENTS, clients);
				page =  ConfigurationManager.getProperty(TAKE_CLIENTS_PAGE);
			}
			
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE,MessageManager.DATABASE_ERROR);
			page =  ConfigurationManager.getProperty(ERROR_PAGE);
		}
		request.getSession().setAttribute(URL, page);
		return page;
	}
}