package by.epam.myshop.command.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.dao.ItemDAO;
import by.epam.myshop.dao.impl.ItemDAOImpl;
import by.epam.myshop.entity.Client;
import by.epam.myshop.entity.Item;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.resource.ConfigurationManager;
import by.epam.myshop.resource.MessageManager;

public class AddItemToBasketCommand implements ActionCommand {
	
	private static final Logger LOG = Logger.getLogger(AddItemToBasketCommand.class);
	private final String AMOUNT = "amount";
	private final String MESSAGE = "message";
	private final String URL = "url";
	private final String ITEM_ID = "item_id";
	private final String CLIENT = "client";
	private final String ITEMS = "items";
	private final String BASKET = "basket";
	private final String AMOUNT_MAP = "amount_map";
	private final String ITEMS_PAGE = "path.page.items";
	

	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("AddItemToBasketCommand.execute()");
		
		String page = null;
		int itemId = Integer.parseInt(request.getParameter(ITEM_ID));
		int amount = Integer.parseInt(request.getParameter(AMOUNT));
		Client client = (Client) request.getSession().getAttribute(CLIENT);
		page = (String) request.getSession().getAttribute(URL);
		List<Item> items = (ArrayList<Item>) request.getSession().getAttribute(ITEMS);
		List<Item> basket = (ArrayList<Item>) request.getSession().getAttribute(BASKET);
		HashMap<Integer, Integer> amountMap = (HashMap<Integer, Integer>) request.getSession().getAttribute(AMOUNT_MAP);
		if (amountMap == null) {
			amountMap = new HashMap<>();
		}
		
		LOG.info("itemID = " + itemId);
		LOG.info(items.toString());
		
		try {
			ItemDAO itemDAO = ItemDAOImpl.getInstance();
			if (basket == null) {
				basket = new ArrayList<Item>();
			} else {
				for (Item item : basket) {
					if (item.getItemId() == itemId) {
						request.setAttribute(MESSAGE, MessageManager.ADD_ITEM_TO_BASKET_MORE_ONE_ERROR_NO_CLIENT);
						items = itemDAO.findAll();
						request.setAttribute(ITEMS, items);
						request.getSession().setAttribute(URL, page);
						page = ConfigurationManager.getProperty(ITEMS_PAGE); 
						return page;
					}
				}
			}
			if (client != null) {
				Item item = itemDAO.findEntityById(itemId);
				if (item != null && item.getItemsAmount() >= amount) {
					basket.add(item);
					item.setItemsAmount(item.getItemsAmount() - amount); 
					itemDAO.modifyItemInfo(item);
					amountMap.put(item.getItemId(), amount);
					
					request.getSession().setAttribute(AMOUNT_MAP, amountMap);					
					request.getSession().setAttribute(BASKET, basket);
					
					items = itemDAO.findAll();
					request.setAttribute(MESSAGE, MessageManager.SUCCESSFUL_ADD_ITEM_TO_BASKET);

				} else {
					request.setAttribute(MESSAGE, MessageManager.ADD_ITEM_TO_BASKET_ERROR);
					page = ConfigurationManager.getProperty(ITEMS_PAGE); 
					return page;
				}
			} else {
				request.setAttribute(MESSAGE, MessageManager.ADD_ITEM_TO_BASKET_CLIENT_NOT_EXISTS_ERROR);
				page = ConfigurationManager.getProperty(ITEMS_PAGE); 
				return page;
			}
		} catch (DAOException e) {
			LOG.error("DAOException", e);
			request.setAttribute(MESSAGE, MessageManager.DATABASE_ERROR);
			page = ConfigurationManager.getProperty(ITEMS_PAGE);
		}
		LOG.info(items.toString());
		request.setAttribute(ITEMS, items);
		request.getSession().setAttribute(URL, page);
		return page;
	}
}
