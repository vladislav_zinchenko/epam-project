<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>
<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.signup"/></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
    <script src="js/ie-emulation-modes-warning.js"></script>
	
  </head>

  <body>
  <!-- Navigation -->
	<%@include file="/jsp/jspf/login_nav.jspf"%>
	<!---->
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->
	
	<div class="container-log">
	
      <form class="form" name="SignUpInForm" method="POST" action="Controller">
        <h2 class="form-heading">
        	<fmt:message key="static.signUp"/>
        </h2>
		
		<input type="text" class="form-control" placeholder="<fmt:message key="name"/>" name="name" pattern="[А-Яа-яA-Za-z]{2,30}" required>
		<input type="text" class="form-control" placeholder="<fmt:message key="surname"/>" name="surname" pattern="[А-Яа-яA-Za-z]{2,30}" required>
        <input type="text"  class="form-control" placeholder="<fmt:message key="login"/>" name="login" pattern="[\wА-Яа-я]{1,30}" required>
        <input type="password" class="form-control" placeholder="<fmt:message key="password"/>" name="password" pattern="[\w]{3,30}" required> 
		
		<input type="hidden" name="command" value="signup"/>
        <input class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="signUp"/>">
      </form>
	  
	  <form class="form" name="ToLoginForm" method="POST" action="Controller">
		<input type="hidden" name="command" value="toindex"/>
		<input class="btn btn-lg btn-primary btn-block" type="submit" value="<fmt:message key="toLogIn"/>"/>
	  </form>
	  
    </div>
	
	<%@include file="/jsp/jspf/footer.jspf"%>
  
      <script src="js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>