package by.epam.myshop.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.myshop.command.ActionCommand;

/**
 * Uses when needed to change language.
 */

public class ChangeLocaleCommand implements ActionCommand {
	private final String URL = "url";
	private final String LOCALE = "locale";
	private static final Logger LOG = Logger.getLogger(ChangeLocaleCommand.class);
	
	@Override
	public String execute(HttpServletRequest request) {
		LOG.info("ChangeLocaleCommand.getAttribute(URL): " + request.getSession().getAttribute(URL));
		
		String language = request.getParameter(LOCALE);
		request.getSession().setAttribute(LOCALE, language);
		String page = (String) request.getSession().getAttribute(URL);
		return page;
	}
}
