package by.epam.myshop.dao.resource;

import java.util.ResourceBundle;

public class DBResourceManager {
	
	private final static String PROPERTY_FILE = "db"; // resources/db
	private final static DBResourceManager instance = new DBResourceManager();
	
	private static ResourceBundle bundle = 
			ResourceBundle.getBundle(PROPERTY_FILE);
	
	public static DBResourceManager getInstance() {
		return instance;
	}
	
	public static String getValue(String key) {
		return bundle.getString(key);
	}
}
