package by.epam.myshop.command.enumeration;

import by.epam.myshop.command.ActionCommand;
import by.epam.myshop.command.admin.AddItemCommand;
import by.epam.myshop.command.admin.ChangeBlackListStatusCommand;
import by.epam.myshop.command.admin.DeleteClientCommand;
import by.epam.myshop.command.admin.DeleteItemCommand;
import by.epam.myshop.command.admin.FinishOrderCommand;
import by.epam.myshop.command.admin.ModifyItemCommand;
import by.epam.myshop.command.admin.TakeClientsCommand;
import by.epam.myshop.command.direction.ToAddItemCommand;
import by.epam.myshop.command.direction.ToBasketCommand;
import by.epam.myshop.command.direction.ToIndexCommand;
import by.epam.myshop.command.direction.ToModifyItemCommand;
import by.epam.myshop.command.direction.ToSignUpCommand;
import by.epam.myshop.command.impl.AddItemToBasketCommand;
import by.epam.myshop.command.impl.ChangeLocaleCommand;
import by.epam.myshop.command.impl.DeleteItemFromBasketCommand;
import by.epam.myshop.command.impl.DeleteOrderCommand;
import by.epam.myshop.command.impl.LogInCommand;
import by.epam.myshop.command.impl.LogOutCommand;
import by.epam.myshop.command.impl.OrderFromBasketCommand;
import by.epam.myshop.command.impl.SignUpCommand;
import by.epam.myshop.command.impl.TakeItemInfoCommand;
import by.epam.myshop.command.impl.TakeItemsByTypeCommand;
import by.epam.myshop.command.impl.TakeItemsCommand;
import by.epam.myshop.command.impl.TakeOrdersCommand;

public enum CommandEnum {
	LOGIN {
		{
			this.command = new LogInCommand();
		}
	},
	LOGOUT {
		{
			this.command = new LogOutCommand();
		}
	},
	TOSIGNUP {
		{
			this.command = new ToSignUpCommand();
		}
	},
	SIGNUP {
		{
			this.command = new SignUpCommand();
		}
	},
	CHANGE_LOCALE {
		{
			this.command = new ChangeLocaleCommand();
		}
	},
	TOINDEX {
		{
			this.command = new ToIndexCommand();
		}
	},
	TAKE_ITEMS {
		{
			this.command = new TakeItemsCommand();
		}
	},
	TO_ADD_ITEM {
		{
			this.command = new ToAddItemCommand();
		}
	},
	ADD_ITEM {
		{
			this.command = new AddItemCommand();
		}
	},
	DELETE_ITEM {
		{
			this.command = new DeleteItemCommand();
		}
	},
	MODIFY_ITEM {
		{
			this.command = new ModifyItemCommand();
		}
	},
	TO_MODIFY_ITEM {
		{
			this.command = new ToModifyItemCommand();
		}
	},
	TAKE_CLIENTS {
		{
			this.command = new TakeClientsCommand();
		}
	},
	TAKE_ITEM_INFO {
		{
			this.command = new TakeItemInfoCommand();
		}
	},
	ADD_ITEM_TO_BASKET {
		{
			this.command = new AddItemToBasketCommand();
		}
	},
	TO_BASKET {
		{
			this.command = new ToBasketCommand();
		}
	},
	DELETE_ITEM_FROM_BASKET {
		{
			this.command = new DeleteItemFromBasketCommand();
		}
	},
	ORDER_ITEMS
	{
		{
			this.command = new OrderFromBasketCommand();
		}
	},
	TO_ORDER
	{
		{
			this.command = new TakeOrdersCommand();
		}
	},
	TAKE_ITEMS_BY_TYPE
	{
		{
			this.command = new TakeItemsByTypeCommand();
		}
	},
	DELETE_ORDER
	{
		{
			this.command = new DeleteOrderCommand();
		}
	},
	DELETE_CLIENT
	{
		{
			this.command = new DeleteClientCommand();
		}
	},
	FINISH_ORDER
	{
		{
			this.command = new FinishOrderCommand();
		}
	},
	BLACKLIST_CLIENT
	{
		{
			this.command = new ChangeBlackListStatusCommand();
		}
	};
	
	ActionCommand command;

	public ActionCommand getCurrentCommand() {
		return command;
	}
}