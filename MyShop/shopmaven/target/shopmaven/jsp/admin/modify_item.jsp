<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>

<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.items"/></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
    <script src="js/ie-emulation-modes-warning.js"></script>
	
  </head>

  <body>
	<!-- Navigation for user-->
	<%@include file="/jsp/jspf/ind_nav.jspf"%>
	<!---->
	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->
	

	<div class="container-main-info" padding="20px">

		<form id="modifyform"  method="POST" action="Controller">
		<div class="form-group form-group-lg">
			<label for="name" id="font"><fmt:message key="static.add.item.name"/></label>
			<input type="text" class="form-control" id="name" name="name" value="${item.itemName}" pattern="[А-ЯA-Zа-яa-z\\s- 0-9]{1,45}" required>
		</div>
		<div class="form-group form-group-lg">
			<label for="type" id="font"><fmt:message key="static.add.item.type"/></label>
			<select class="form-control" id="type" name="type" required>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
		</div>
		<div class="form-group form-group-lg">
			<label for="amount" id="font"><fmt:message key="static.add.item.amount"/></label>
			<input type="text" class="form-control" id="amount" name="amount" value="${item.itemsAmount}" pattern="[1-9][0-9]{0,1}" required>
		</div>
		<div class="form-group form-group-lg">
			<label for="desc" id="font"><fmt:message key="static.add.item.desc"/></label>
			<textarea class="form-control" rows="3"	id="desc" name="description" value="${item.itemDescription}" pattern="[А-ЯA-Zа-яa-z\\s-]{2,45}" required></textarea>
		</div>
		<div class="form-group form-group-lg">
			<label for="path" id="font"><fmt:message key="static.add.item.pic"/></label>
			<input type="text" class="form-control" id="path" name="path" value="${item.itemPicPath}" pattern="[А-ЯA-Zа-яa-z0-9\\s-\\:./]{2,100}" required>
		</div>
		<div class="form-group form-group-lg">
			<label for="manufacter" id="font"><fmt:message key="static.add.item.manufacter"/></label>
			<input type="text" class="form-control" id="manufacter" name="manufacter" value="${item.manufacterName}" pattern="[А-ЯA-Zа-яa-z\\s]{2,45}" required>
		</div>
		
		<input type="hidden" name="item_id" value="${item.itemId}" />
		<!-- Подумать насчет перечисления для произодителя-->
		<input type="hidden" name="command" value="modify_item"/>
		<input class="btn btn-lg btn-default btn-block" type="submit" value="<fmt:message key="button.item.modify"/> "/>
		
	</form>
	</div>
	
	
	 <%@include file="/jsp/jspf/footer.jspf"%>
  </body>
</html>