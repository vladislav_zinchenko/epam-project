<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/jsp/jspf/bundle.jspf"%>

<html>
	<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.login"/></title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/css/signin.css" rel="stylesheet" type="text/css">
	
    <script src="js/ie-emulation-modes-warning.js"></script>
	
  </head>

  <body>
	<!--need
	<c:if test="${not empty sessionScope.client}">
		<jsp:forward page="/jsp/b_main.jsp" />
	</c:if>
	<!-- Navigation -->
	<%@include file="/jsp/jspf/login_nav.jspf"%>

	<!-- Message -->
	<%@include file="/jsp/jspf/message.jspf"%>
	<!---->
	<!-- Forms -->
    <div class="container-log">
	
      <form id="form-login" class="form" name="LogInForm" method="POST" action="Controller" margin="20px">
        <h2 class="form-heading">
			<fmt:message key="static.login"/>
		</h2>
		
        <input type="text"  class="form-control" placeholder="<fmt:message key="login"/>" name="login" pattern="[\wА-Яа-я]{1,30}" required>
        <input type="password" class="form-control" placeholder="<fmt:message key="password"/>" name="password"  required>
		<input type="hidden" name="command" value="login"/>
        <input class="btn btn-lg btn-default btn-block" type="submit" value="<fmt:message key="logIn"/>">
      </form>
	  
	  <form class="form" name="ToSignUpForm" method="POST" action="Controller">
		<input type="hidden" name="command" value="tosignup"/>
		<input class="btn btn-lg btn-default btn-block" type="submit" value="<fmt:message key="toSignUp"/> "/>
	  </form>
	  
    </div>
	
	<%@include file="/jsp/jspf/footer.jspf"%>
	
    <script src="js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>