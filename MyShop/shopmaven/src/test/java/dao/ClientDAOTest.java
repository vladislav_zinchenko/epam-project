package dao;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import by.epam.myshop.dao.ClientDAO;
import by.epam.myshop.dao.impl.ClientDAOImpl;
import by.epam.myshop.dao.pool.DBConnectionPool;
import by.epam.myshop.entity.Client;
import by.epam.myshop.exception.ConnectionPoolException;
import by.epam.myshop.exception.DAOException;
import by.epam.myshop.util.Coder;
import junit.framework.Assert;

@SuppressWarnings("deprecation")
public class ClientDAOTest {
	
	private static final Logger LOG = Logger.getLogger(ClientDAOTest.class);
	private static ClientDAO clientDAO = ClientDAOImpl.getInstance();
	
	@BeforeClass
	public static void initializePool() {
		try {
			DBConnectionPool.getInstance().initializeAvailableConnection();
		} catch (ConnectionPoolException e) {
			throw new RuntimeException("Error while initializing connection pool", e);
		}
	}

	@Test
	@Before
	public void testSignUp() {
		LOG.info("testSignUp()");
		
        Client client = new Client();
        client.setName("����");
        client.setLogin("hanna");
        client.setSurname("��������");
        client.setStatus(0);
        client.setStatusBL(0);
        client.setPassword(Coder.hashMD5("123"));
        client.setId(Math.abs(client.hashCode()));
        try {
            clientDAO.signUp(client);
            String login = client.getLogin();
            Assert.assertNotNull(login);
            String password = client.getPassword();
            Assert.assertNotNull(password);
            
            Client newClient = clientDAO.findClientByLoginAndPassword(login, password);
            Assert.assertEquals(client.getLogin(), newClient.getLogin());
            Assert.assertEquals(client.getName(), newClient.getName());
            Assert.assertEquals(client.getSurname(), newClient.getSurname());
            Assert.assertEquals(client.getStatus(), newClient.getStatus());
            Assert.assertEquals(client.getStatus(), newClient.getStatusBL());
            Assert.assertEquals(client.getPassword(), newClient.getPassword());
            Assert.assertEquals(client.getId(), newClient.getId());
        } catch (DAOException e) {
            LOG.error("DAOException", e);
        }
        return;
	}
	
	@Test
	public void testFindClientByLoginAndPassword() {
		LOG.info("testFindClientByLoginAndPassword()");
		
		Client client = new Client();
        client.setName("����");
        client.setSurname("��������");
        client.setStatus(0);
        client.setStatusBL(0);
        client.setPassword(Coder.hashMD5("123"));
		try {
			Client newClient = clientDAO.findClientByLoginAndPassword("hanna", Coder.hashMD5("123"));
			
			Assert.assertEquals(client.getName(), newClient.getName());
			Assert.assertEquals(client.getSurname(), newClient.getSurname());
			Assert.assertEquals(client.getStatus(), newClient.getStatus());
			Assert.assertEquals(client.getStatusBL(), newClient.getStatusBL());
		} catch (DAOException e) {
			LOG.error("DAOException", e);
		}
	}

	@Test
	public void testChangeBlackListStatus() {
		LOG.info("testChangeBlackListStatus()");
		
		Client client = new Client();
		client.setLogin("anna");
		client.setName("����");
        client.setSurname("��������");
        client.setStatus(0);
        client.setStatusBL(0);
        client.setPassword(Coder.hashMD5("123"));
        try {
        	clientDAO.signUp(client);
        	String login = client.getLogin();
            Assert.assertNotNull(login);
            String password = client.getPassword();
            Assert.assertNotNull(password);
            int statusBL = client.getStatusBL();
            Assert.assertNotNull(statusBL);
            clientDAO.changeBlackListStatus(login, 1);
            
            Client newClient = clientDAO.findClientByLoginAndPassword(login, password);
            Assert.assertFalse(client.getStatusBL() == newClient.getStatusBL());
        } catch (DAOException e) {
            LOG.error("DAOException", e);
        }
        return;
	}
	
	@Test
	@After
	public void testDeleteClient() {
		LOG.info("testDeleteClient()");
		
        Client client = new Client();
        client.setName("����");
        client.setLogin("hanna");
        client.setSurname("��������");
        client.setStatus(0);
        client.setStatusBL(0);
        client.setPassword(Coder.hashMD5("123"));
        try {
        	clientDAO.signUp(client);
        	String login = client.getLogin();
            Assert.assertNotNull(login);
            String password = client.getPassword();
            Assert.assertNotNull(password);
            
            clientDAO.delete(login);
            Client newClient = clientDAO.findClientByLoginAndPassword(login, password);
            Assert.assertEquals(null, newClient);
        } catch (DAOException e) {
            LOG.error("DAOException", e);
        }
        return;
	}
	
	@AfterClass
	public static void destroyPool() {
		try {
			DBConnectionPool.getInstance().destroyConnectionPool();
		} catch (ConnectionPoolException e) {
			LOG.error(e);
		}
	}
	
}
